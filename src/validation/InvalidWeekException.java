package validation;

public class InvalidWeekException extends Exception {
    public InvalidWeekException(String message) {
        super(message);
    }
}
