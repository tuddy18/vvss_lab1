package validation;

import domain.Break;

public class BreakValidator implements Validator<Break> {
    @Override
    public void validate(Break entity) throws ValidationException {
        String errors = "";
        if(entity.getID() == null) errors += "Invalid ID!\n";
        if(entity.getStart().isAfter(entity.getFinish())) errors += "Start must be before finish!\n";
        if(entity.getDescription().length() == 0) errors += "Invalid description!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
