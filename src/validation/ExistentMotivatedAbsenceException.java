package validation;

public class ExistentMotivatedAbsenceException extends RuntimeException {
    public ExistentMotivatedAbsenceException(String message) {
        super(message);
    }
}
