package validation;

public class InexistentBreakException extends RuntimeException {

    public InexistentBreakException(String s) {
        super(s);
    }
}
