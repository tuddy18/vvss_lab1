package validation;

import domain.User;

public class UserValidator implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {
        String errors = "";
        if(entity.getID() == null) errors += "Invalid username!\n";
        if(entity.getPassword() == null) errors += "Invalid password!\n";
        if(entity.getUserRole() == null) errors += "Invalid role!\n";
        if(entity.getName() == null) errors += "Invalid name!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
