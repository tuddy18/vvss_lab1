package validation;

public class ExistentGradeException extends RuntimeException {

    public ExistentGradeException(String message) {
        super(message);
    }
}
