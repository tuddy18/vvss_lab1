package validation;

import domain.MotivatedAbsence;

public class MotivatedAbsenceValidator implements Validator<MotivatedAbsence> {
    @Override
    public void validate(MotivatedAbsence entity) throws ValidationException {
        String errors = "";
        if(entity.getID() < 0) errors += "Invalid ID!\n";
        if(entity.getWeek() < 1 || entity.getWeek() > 14) errors += "Invalid week!\n";
        if(entity.getStudent() == null) errors += "Invalid student!\n";
        if(entity.getReason().isEmpty()) errors += "Invalid reason!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
