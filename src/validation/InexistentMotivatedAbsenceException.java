package validation;

public class InexistentMotivatedAbsenceException extends RuntimeException {
    public InexistentMotivatedAbsenceException(String s) {
        super(s);
    }
}
