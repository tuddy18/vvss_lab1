package validation;

public class ExistentHomeworkException extends RuntimeException {
    public ExistentHomeworkException(String message) {
        super(message);
    }
}
