package validation;

public class InvalidChangeException extends RuntimeException {
    public InvalidChangeException(String message) {
        super(message);
    }
}
