package validation;

public class InvalidNumberOfParametersException extends RuntimeException {
    public InvalidNumberOfParametersException(String message) {
        super(message);
    }
}
