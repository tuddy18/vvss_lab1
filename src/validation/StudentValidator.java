package validation;

import domain.Student;
import domain.StudentContants;

public class StudentValidator implements Validator<Student> {
    @Override
    public void validate(Student entity) throws ValidationException {
        String errors = "";

        if(entity.getID() < 0) errors += "Invalid ID!\n";
        if(!entity.getName().matches("[a-zA-z ]+")) errors += "Invalid name!\n";
        short group = entity.getGroup();
        if(group / 1000 > 0 ||
                !(group / 100 >= StudentContants.MIN_SPECIALIZATION_NR && group / 100 <= StudentContants.MAX_SPECIALIZATION_NR) ||
                !(group / 10 % 10 >= StudentContants.MIN_YEAR_NR && group / 10 % 10 <= StudentContants.MAX_YEAR_NR) ||
                !(group % 10 >= StudentContants.MIN_GROUP_NR && group % 10 <= StudentContants.MAX_GROUP_NR))
            errors += "Invalid group!\n";
        if(!entity.getEmail().matches("[a-z]{4}[0-9]{4}@scs.ubbcluj.ro")) errors += "Invalid email!\n";
        if(!entity.getLabTeacherName().matches("[a-zA-z ]+")) errors += "Invalid teacher name!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
