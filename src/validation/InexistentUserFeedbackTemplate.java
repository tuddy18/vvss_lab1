package validation;

public class InexistentUserFeedbackTemplate extends RuntimeException {
    public InexistentUserFeedbackTemplate(String message) {
        super(message);
    }
}
