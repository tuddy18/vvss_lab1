package validation;

public class ExistentStudentException extends RuntimeException {
    public ExistentStudentException(String message) {
        super(message);
    }
}
