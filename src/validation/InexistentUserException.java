package validation;

public class InexistentUserException extends RuntimeException {
    String message;
    public InexistentUserException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
