package validation;

import domain.Grade;

public class GradeValidator implements Validator<Grade> {
    @Override
    public void validate(Grade entity) throws ValidationException {
        String errors = "";
        if(entity.getValue() < 0 || entity.getValue() > 10) errors += "Invalid grade value!\n";
        if(entity.getStudent() == null) errors += "Invalid student!\n";
        if(entity.getHomework() == null) errors += "Invalid homework!\n";
        if(entity.getID() == null || entity.getID() < 0) errors += "Invalid id!\n";

        if(!errors.isEmpty())
            throw new ValidationException(errors);
    }
}
