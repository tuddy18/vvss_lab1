package validation;

public class OverlappingBreaksException extends RuntimeException {
    @Override
    public String getMessage() {
        return super.getMessage();
    }

    public OverlappingBreaksException(String s) {
        super(s);
    }
}
