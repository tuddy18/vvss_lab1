package validation;

import domain.UserFeedbackTemplate;

public class UserFeedbackValidator implements Validator<UserFeedbackTemplate> {
    @Override
    public void validate(UserFeedbackTemplate entity) throws ValidationException {
        String errors = "";
        if(entity.getID() == null || entity.getID().length() == 0) errors += "Invalid username!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
