package validation;

public class ExistentUserException extends RuntimeException {
    String message;
    public ExistentUserException(String message) {
        super(message);
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
