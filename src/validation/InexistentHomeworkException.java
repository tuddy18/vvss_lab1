package validation;

public class InexistentHomeworkException extends RuntimeException {
    public InexistentHomeworkException(String message) {
        super(message);
    }
}
