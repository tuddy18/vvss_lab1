package validation;

import domain.Homework;

public class HomeworkValidator implements Validator<Homework> {
    @Override
    public void validate(Homework entity) throws ValidationException {
        String errors = "";

        if(entity.getID() < 0) errors += "Invalid ID!\n";
        if(entity.getAssignedWeek() < 1 || entity.getAssignedWeek() > 14) errors += "Invalid assigned week!\n";
        if(entity.getDeadlineWeek() < 1 || entity.getDeadlineWeek() > 14) errors += "Invalid deadline week!\n";
        if(entity.getAssignedWeek() >= entity.getDeadlineWeek()) errors += "Invalid period for doing the homework!\n";
        if(entity.getDescription().length() == 0) errors += "Invalid description!\n";

        if(errors.length() > 0)
            throw new ValidationException(errors);
    }
}
