package validation;

public class InexistentGradeException extends RuntimeException {
    public InexistentGradeException(String message) {
        super(message);
    }
}
