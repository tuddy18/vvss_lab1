package validation;

public class InexistentStudentException extends RuntimeException {
    public InexistentStudentException(String message) {
        super(message);
    }
}
