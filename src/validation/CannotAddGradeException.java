package validation;

public class CannotAddGradeException extends RuntimeException {
    public CannotAddGradeException(String message) {
        super(message);
    }
}
