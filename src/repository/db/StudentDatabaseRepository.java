package repository.db;

import domain.Student;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class StudentDatabaseRepository extends AbstractDatabaseRepository<Integer, Student> {
    private String table;
    public StudentDatabaseRepository(String table, Validator<Student> validator, Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
    }

    @Override
    Student extractEntity(ResultSet resultSet) throws SQLException {
        return new Student(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getShort(3), resultSet.getString(4), resultSet.getString(5));
    }

    @Override
    Statement insertEntity(Connection connection, Student entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getID());
        statement.setString(2, entity.getName());
        statement.setShort(3, entity.getGroup());
        statement.setString(4, entity.getEmail());
        statement.setString(5, entity.getLabTeacherName());
        return statement;
    }


    @Override
    Statement updateEntity(Connection connection, Student entity) throws SQLException {
        String sql = "UPDATE " + table + " SET name = ?, groupp = ?, email = ?, labTeacher = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, entity.getName());
        statement.setShort(2, entity.getGroup());
        statement.setString(3, entity.getEmail());
        statement.setString(4, entity.getLabTeacherName());
        statement.setInt(5, entity.getID());
        return statement;
    }

    @Override
    String getColumns() {
        return "(ID NUMERIC(18), name VARCHAR(50), groupp SMALLINT, email VARCHAR(50), labTeacher VARCHAR(50))";
    }
}
