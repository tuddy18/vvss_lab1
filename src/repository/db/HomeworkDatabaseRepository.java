package repository.db;

import domain.Homework;
import repository.db.AbstractDatabaseRepository;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class HomeworkDatabaseRepository extends AbstractDatabaseRepository<Integer, Homework> {
    private String table;
    public HomeworkDatabaseRepository(String table, Validator<Homework> validator, Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
    }

    @Override
    Homework extractEntity(ResultSet resultSet) throws SQLException {
        return new Homework(resultSet.getInt(1), resultSet.getString(2),
                resultSet.getByte(3), resultSet.getByte(4));
    }

    @Override
    Statement insertEntity(Connection connection, Homework entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getID());
        statement.setString(2, entity.getDescription());
        statement.setByte(3, entity.getAssignedWeek());
        statement.setByte(4, entity.getDeadlineWeek());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, Homework entity) throws SQLException {
        String sql = "UPDATE " + table + " SET description = ?, assignition = ?, deadline = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, entity.getDescription());
        statement.setByte(2, entity.getAssignedWeek());
        statement.setByte(3, entity.getDeadlineWeek());
        statement.setInt(4, entity.getID());
        return statement;
    }

    @Override
    String getColumns() {
        return "(ID NUMERIC(18), description VARCHAR(50), assignition BIT(8), deadline BIT(8))";
    }
}
