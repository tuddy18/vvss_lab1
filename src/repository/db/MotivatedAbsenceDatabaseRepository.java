package repository.db;

import domain.MotivatedAbsence;
import domain.Student;
import repository.CrudRepository;
import repository.db.AbstractDatabaseRepository;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class MotivatedAbsenceDatabaseRepository extends AbstractDatabaseRepository<Integer, MotivatedAbsence> {
    private String table;
    private CrudRepository<Integer, Student> studentCrudRepository;
    public MotivatedAbsenceDatabaseRepository(String table, Validator<MotivatedAbsence> validator,
                                              CrudRepository<Integer, Student> studentCrudRepository,
                                              Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
        this.studentCrudRepository = studentCrudRepository;
    }

    @Override
    MotivatedAbsence extractEntity(ResultSet resultSet) throws SQLException {
        Student student = studentCrudRepository.findOne(resultSet.getInt(3));
        if(student == null)
            return null;
        return new MotivatedAbsence(resultSet.getInt(1), student,
                resultSet.getByte(2), resultSet.getString(4));
    }

    @Override
    Statement insertEntity(Connection connection, MotivatedAbsence entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getID());
        statement.setByte(2, entity.getWeek());
        statement.setInt(3, entity.getStudent().getID());
        statement.setString(4, entity.getReason());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, MotivatedAbsence entity) throws SQLException {
        String sql = "UPDATE " + table + " SET week = ?, studentID = ?, reason = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setByte(1, entity.getWeek());
        statement.setInt(2, entity.getStudent().getID());
        statement.setString(3, entity.getReason());
        statement.setInt(4, entity.getID());
        return statement;
    }

    @Override
    String getColumns() {
        return "(ID NUMERIC(18), week BIT(8), studentID NUMERIC(18), reason VARCHAR(50))";
    }
}
