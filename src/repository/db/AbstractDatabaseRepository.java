package repository.db;

import domain.HasID;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;
import org.jasypt.properties.EncryptableProperties;
import repository.CrudRepository;
import validation.ValidationException;

import validation.Validator;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;


public abstract class AbstractDatabaseRepository<ID, E extends HasID<ID>> implements CrudRepository<ID,E> {
    private String database;
    private String user;
    private String password;
    private String findAllQuery;
    private String findOneQuery;
    private String deleteQuery;
    private Validator<E> validator;
    private StandardPBEStringEncryptor encryptor;

    private void setupLogin() throws IOException {
        //System.out.println(encryptor.encrypt("admin"));
        Properties prop = new EncryptableProperties(encryptor);
        InputStream inputStream = getClass().getResourceAsStream("/resources/data/config.properties");
        if (inputStream != null) {
            prop.load(inputStream);
        } else {
            throw new FileNotFoundException("Properties file missing");
        }

        database = prop.getProperty("database");
        user = prop.getProperty("user");
        password = prop.getProperty("password").replaceAll(" ","+");
        inputStream.close();
    }

    protected String decrypt(String s) {
        return encryptor.decrypt(s);
    }

    protected String encrypt(String s) {
        return encryptor.encrypt(s);
    }

    public AbstractDatabaseRepository(String table, Validator<E> validator, Boolean createTable) throws IOException {
        this.validator = validator;
        encryptor = new StandardPBEStringEncryptor();
        String key = System.getenv("encryptorKey");
        encryptor.setPassword(key);
        setupLogin();
        findAllQuery = "SELECT * FROM " + table;
        findOneQuery = "SELECT * FROM " + table + " WHERE ID = ?";
        deleteQuery = "DELETE FROM " + table + " WHERE ID = ?";

        if(createTable)
            try (
                    Connection connection = DriverManager.getConnection(
                            database, user, password);
                    Statement statement = connection.createStatement();
            ) {
                statement.executeUpdate("CREATE TABLE " + table + " " + getColumns());
            } catch (SQLException e) {
                //System.out.println(e.getErrorCode());
                if(e.getErrorCode() != 2714) //existent table, no worry
                    e.printStackTrace();
            }
    }

    @Override
    public E findOne(ID id) {
        E entity = null;
        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
                PreparedStatement statement = connection.prepareStatement(findOneQuery, Statement.RETURN_GENERATED_KEYS);
        ) {
            //System.out.println(findOneQuery + id);
            statement.setString(1, id.toString());
            ResultSet resultSet = statement.executeQuery();
            if(resultSet.next())
                entity = extractEntity(resultSet);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }


    @Override
    public Iterable<E> findAll() {
        List<E> resultList = new ArrayList<>();
        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
                Statement statement = connection.createStatement();
        ) {
            ResultSet resultSet = statement.executeQuery(findAllQuery);
            while(resultSet.next()) {
                E entity = extractEntity(resultSet);
                if(entity != null)
                    resultList.add(entity);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @Override
    public E save(E entity) throws ValidationException {
        validator.validate(entity);
        int rows = 0;
        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
        ) {
            PreparedStatement statement = (PreparedStatement) insertEntity(connection, entity);
            rows = statement.executeUpdate();
        } catch (SQLException e) {
            //System.out.println(e.getErrorCode());
            if(e.getErrorCode() != 2627) //existent entity (pk violation)
                e.printStackTrace();
        }
        return rows == 0 ? entity : null;
    }

    @Override
    public E delete(ID id) {
        E entity = findOne(id);
        if(entity == null)
            return null;

        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
                PreparedStatement statement = connection.prepareStatement(deleteQuery, Statement.RETURN_GENERATED_KEYS);
        ) {
            /*statement.executeQuery(deleteQuery + id + "'");
            ResultSet resultSet = statement.getResultSet();
            if(resultSet.next())
                entity = extractEntity(resultSet);*/
            statement.setString(1, id.toString());
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return entity;
    }

    @Override
    public E update(E entity) throws ValidationException {
        validator.validate(entity);
        E oldEntity = findOne(entity.getID());
        int rows = 0;
        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
        ) {
            PreparedStatement statement = (PreparedStatement) updateEntity(connection, entity);
            rows = statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rows == 0 ? null : oldEntity;
    }

    public Iterable<String> getAllDatabaseTableNames(String pattern) {
        List<String> tables = new ArrayList<>();
        try (
                Connection connection = DriverManager.getConnection(
                        database, user, password);
        ) {
            DatabaseMetaData md = connection.getMetaData();
            ResultSet rs = md.getTables(null, "dbo", pattern == null ? "%" : pattern, null);
            while (rs.next()) {
               tables.add(rs.getString(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tables;
    }

    abstract E extractEntity(ResultSet resultSet) throws SQLException;
    abstract Statement insertEntity(Connection connection, E entity) throws SQLException;
    abstract Statement updateEntity(Connection connection, E entity) throws SQLException;
    abstract String getColumns();
}
