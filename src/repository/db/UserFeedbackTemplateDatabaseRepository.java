package repository.db;

import domain.UserFeedbackTemplate;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class UserFeedbackTemplateDatabaseRepository extends AbstractDatabaseRepository<String, UserFeedbackTemplate> {
    String table;
    public UserFeedbackTemplateDatabaseRepository(String table, Validator<UserFeedbackTemplate> validator, Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
    }

    @Override
    UserFeedbackTemplate extractEntity(ResultSet resultSet) throws SQLException {
        return new UserFeedbackTemplate(resultSet.getString(1), resultSet.getString(2));
    }

    @Override
    Statement insertEntity(Connection connection, UserFeedbackTemplate entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, entity.getID());
        statement.setString(2, entity.getFeedbackTemplate());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, UserFeedbackTemplate entity) throws SQLException {
        String sql = "UPDATE " + table + " SET feedbackTemplate = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(2, entity.getID());
        statement.setString(1, entity.getFeedbackTemplate());
        return statement;
    }

    @Override
    String getColumns() {
        return "(username VARCHAR(50), feedbackTemplate VARCHAR(50))";
    }
}
