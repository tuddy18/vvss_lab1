package repository.db;

import domain.Break;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class BreakDatabaseRepository extends AbstractDatabaseRepository<Integer, Break> {
    String table;
    public BreakDatabaseRepository(String table, Validator<Break> validator, Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
    }

    @Override
    Break extractEntity(ResultSet resultSet) throws SQLException {
        return new Break(resultSet.getInt(1),
                resultSet.getDate(2).toLocalDate(),
                resultSet.getDate(3).toLocalDate(),
                resultSet.getString(4));
    }

    @Override
    Statement insertEntity(Connection connection, Break entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getID());
        statement.setDate(2, Date.valueOf(entity.getStart()));
        statement.setDate(3, Date.valueOf(entity.getFinish()));
        statement.setString(4, entity.getDescription());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, Break entity) throws SQLException {
        String sql = "UPDATE " + table + " SET start = ?, finish = ?, description = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(4, entity.getID());
        statement.setDate(1, Date.valueOf(entity.getStart()));
        statement.setDate(2, Date.valueOf(entity.getFinish()));
        statement.setString(3, entity.getDescription());
        return statement;
    }

    @Override
    String getColumns() {
        return "(ID NUMERIC(18), start Date, finish Date, description VARCHAR(50))";
    }
}
