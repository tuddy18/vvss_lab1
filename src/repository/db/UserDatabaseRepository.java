package repository.db;

import domain.User;
import utilities.UserRole;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class UserDatabaseRepository extends AbstractDatabaseRepository<String, User> {
    private String table;
    public UserDatabaseRepository(String table, Validator<User> validator, Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
    }

    @Override
    User extractEntity(ResultSet resultSet) throws SQLException {
        return new User(resultSet.getString(1), decrypt(resultSet.getString(2)),
                UserRole.valueOf(resultSet.getString(3)), resultSet.getString(4));
    }

    @Override
    Statement insertEntity(Connection connection, User entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, entity.getID());
        statement.setString(2, encrypt(entity.getPassword()));
        statement.setString(3, entity.getUserRole().toString());
        statement.setString(4, entity.getName());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, User entity) throws SQLException {
        String sql = "UPDATE " + table + " SET password = ?, role = ?, name = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, encrypt(entity.getPassword()));
        statement.setString(2, entity.getUserRole().toString());
        statement.setString(3, entity.getName());
        statement.setString(4, entity.getID());
        return statement;
    }

    @Override
    String getColumns() {
        return "(username VARCHAR(50), password VARCHAR(50), userrole VARCHAR(50), name VARCHAR(50))";
    }
}
