package repository.db;

import domain.Grade;
import domain.Homework;
import domain.Student;
import repository.CrudRepository;
import repository.db.AbstractDatabaseRepository;
import validation.Validator;

import java.io.IOException;
import java.sql.*;

public class GradeDatabaseRepository extends AbstractDatabaseRepository<Integer, Grade> {
    private String table;
    private CrudRepository<Integer, Student> studentCrudRepository;
    private CrudRepository<Integer, Homework> homeworkCrudRepository;
    public GradeDatabaseRepository(String table, Validator<Grade> validator,
                                   CrudRepository<Integer, Student> studentCrudRepository,
                                   CrudRepository<Integer, Homework> homeworkCrudRepository,
                                   Boolean createTable) throws IOException {
        super(table, validator, createTable);
        this.table = table;
        this.studentCrudRepository = studentCrudRepository;
        this.homeworkCrudRepository = homeworkCrudRepository;
    }

    @Override
    Grade extractEntity(ResultSet resultSet) throws SQLException {
        Student student = studentCrudRepository.findOne(resultSet.getInt(2));
        Homework homework = homeworkCrudRepository.findOne(resultSet.getInt(3));
        if(student == null || homework == null)
            return null;
        return new Grade(resultSet.getDouble(4), student, homework, resultSet.getByte(5),
                resultSet.getString(6));
    }

    @Override
    Statement insertEntity(Connection connection, Grade entity) throws SQLException {
        String sql = "INSERT INTO " + table + " VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getID());
        statement.setInt(2, entity.getStudent().getID());
        statement.setInt(3, entity.getHomework().getID());
        statement.setDouble(4, entity.getValue());
        statement.setByte(5, entity.getSubmittedWeek());
        statement.setString(6, entity.getFeedback());
        return statement;
    }

    @Override
    Statement updateEntity(Connection connection, Grade entity) throws SQLException {
        String sql = "UPDATE " + table + " SET studentID = ?, homeworkID = ?, value = ?, submittedWeek = ?, feedback = ? WHERE ID = ?";
        PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setInt(1, entity.getStudent().getID());
        statement.setInt(2, entity.getHomework().getID());
        statement.setDouble(3, entity.getValue());
        statement.setByte(4, entity.getSubmittedWeek());
        statement.setString(5, entity.getFeedback());
        statement.setInt(6, entity.getID());
        return statement;
    }

    @Override
    String getColumns() {
        return "(ID int primary key, studentID int not null, homeworkID int not null, value float not null, " +
                "submittedWeek tinyint not null, feedback varchar(200))";
    }
}
