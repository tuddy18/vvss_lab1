package repository;

import domain.HasID;
import validation.ValidationException;
import validation.Validator;

import java.util.HashMap;
import java.util.Map;

public class InMemoryRepository <ID, E extends HasID<ID>> implements CrudRepository <ID, E> {
    
    protected Map<ID, E> entities;
    private Validator<E> validator;

    public InMemoryRepository(Validator<E> validator) {
        this.validator = validator;
        entities = new HashMap<>();
    }

    @Override
    public E findOne(ID id) {
        if(id == null)
            throw new IllegalArgumentException("ID cannot pe null!");
        return entities.get(id);
    }

    @Override
    public Iterable<E> findAll() {
        return entities.values();
    }

    @Override
    public E save(E entity) throws ValidationException {
        if(entity == null)
            throw new IllegalArgumentException("Entity cannot pe null!");
        validator.validate(entity);
        /*E result = entities.get(entity.getStudentID());
        if(result == null)
            entities.put(entity.getStudentID(), entity);
        return result;*/
        return entities.putIfAbsent(entity.getID(), entity);
    }

    @Override
    public E delete(ID id) {
        return entities.remove(id);
    }

    @Override
    public E update(E entity) throws ValidationException {
        if(entity == null)
            throw new IllegalArgumentException("Entity cannot pe null!");
        validator.validate(entity);
        /*E result = findOne(entity.getStudentID());
        if(result != null)
            delete(entity.getStudentID());
            save(entity);
        return result;*/
        return entities.put(entity.getID(), entity);
    }
}
