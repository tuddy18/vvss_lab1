package repository.xml;

import domain.MotivatedAbsence;
import domain.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import repository.CrudRepository;
import repository.xml.AbstractXMLRepository;
import validation.Validator;

import java.util.function.Function;

public class MotivatedAbsenceXMLRepository extends AbstractXMLRepository<Integer, MotivatedAbsence> {
    private CrudRepository<Integer, Student> studentCrudRepository;

    public MotivatedAbsenceXMLRepository(String fileName, Validator<MotivatedAbsence> validator, CrudRepository<Integer,
                                         Student> studentCrudRepository) {
        super(fileName, validator);
        this.studentCrudRepository = studentCrudRepository;
        readFromFile();
    }

    @Override
    MotivatedAbsence extractEntity(Object e) {
        Element absence = (Element)e;
        Function<String, String> getElement = x -> absence.getElementsByTagName(x).item(0).getTextContent();

        String id = absence.getAttribute("id");
        String studentID = getElement.apply("studentID");
        String week = getElement.apply("week");
        String reason = getElement.apply("reason");

        return new MotivatedAbsence(Integer.valueOf(id), studentCrudRepository.findOne(Integer.valueOf(studentID)),
                Byte.valueOf(week), reason);
    }

    @Override
    Element insertEntity(Document document, MotivatedAbsence entity) {
        Element element = document.createElement("absence");
        element.setAttribute("id", entity.getID().toString());

        Element studentID = document.createElement("studentID");
        studentID.setTextContent(entity.getStudent().getID().toString());
        element.appendChild(studentID);

        Element week = document.createElement("week");
        week.setTextContent(entity.getWeek().toString());
        element.appendChild(week);

        Element reason = document.createElement("reason");
        reason.setTextContent(String.valueOf(entity.getReason()));
        element.appendChild(reason);

        return element;
    }
}
