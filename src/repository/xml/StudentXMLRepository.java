package repository.xml;

import domain.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import validation.Validator;

import java.util.function.Function;

public class StudentXMLRepository extends AbstractXMLRepository<Integer, Student> {
    public StudentXMLRepository(String fileName, Validator<Student> validator) {
        super(fileName, validator);
        readFromFile();
    }

    @Override
    Element insertEntity(Document document, Student s) {
        Element element = document.createElement("student");
        element.setAttribute("id", s.getID().toString());

        Element name = document.createElement("name");
        name.setTextContent(s.getName());
        element.appendChild(name);

        Element group = document.createElement("group");
        group.setTextContent(s.getGroup().toString());
        element.appendChild(group);

        Element email = document.createElement("email");
        email.setTextContent(String.valueOf(s.getEmail()));
        element.appendChild(email);

        Element teacher = document.createElement("teacher");
        teacher.setTextContent(String.valueOf(s.getLabTeacherName()));
        element.appendChild(teacher);

        return element;
    }

    @Override
    Student extractEntity(Object e) {
        Element student = (Element)e;
        Function<String, String> getElement = x -> student.getElementsByTagName(x).item(0).getTextContent();

        String id = student.getAttribute("id");
        String name = getElement.apply("name");
        String group = getElement.apply("group");
        String email = getElement.apply("email");
        String labTeacher = getElement.apply("teacher");

        return new Student(Integer.valueOf(id), name, Short.valueOf(group), email, labTeacher);
    }
}
