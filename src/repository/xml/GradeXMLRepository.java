package repository.xml;

import domain.Grade;
import domain.Homework;
import domain.Student;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import repository.CrudRepository;
import repository.xml.AbstractXMLRepository;
import validation.Validator;

import java.util.function.Function;

public class GradeXMLRepository extends AbstractXMLRepository<Integer, Grade> {

    private CrudRepository<Integer, Homework> homeworkCrudRepository;
    private CrudRepository<Integer, Student> studentCrudRepository;

    public GradeXMLRepository(String fileName, Validator<Grade> gradeValidator, CrudRepository<Integer,
            Student> studentCrudRepository, CrudRepository<Integer, Homework> homeworkCrudRepository) {
        super(fileName, gradeValidator);
        this.studentCrudRepository = studentCrudRepository;
        this.homeworkCrudRepository = homeworkCrudRepository;
        readFromFile();
    }

    @Override
    Grade extractEntity(Object e) {
        Element grade = (Element)e;
        Function<String, String> getElement = x -> grade.getElementsByTagName(x).item(0).getTextContent();

        String homework = getElement.apply("homework");
        String value = getElement.apply("value");
        String submittedWeek = getElement.apply("submittedWeek");
        String deadlineWeek = getElement.apply("deadlineWeek");
        String feedback = getElement.apply("feedback");
        String[] files = fileName.split("/");
        String file = files[files.length - 1].split("\\.")[0];
        Grade g = new Grade(Double.valueOf(value), studentCrudRepository.findOne(Integer.valueOf(
                file.substring(1))),
                homeworkCrudRepository.findOne(Integer.valueOf(homework)), Byte.valueOf(submittedWeek));
        g.setFeedback(feedback);

        return g;
    }

    @Override
    Element insertEntity(Document document, Grade entity) {
        Element element = document.createElement("grade");

        Element homework = document.createElement("homework");
        homework.setTextContent(entity.getHomework().getID().toString());
        element.appendChild(homework);

        Element value = document.createElement("value");
        value.setTextContent(entity.getValue().toString());
        element.appendChild(value);

        Element submittedWeek = document.createElement("submittedWeek");
        submittedWeek.setTextContent(entity.getSubmittedWeek().toString());
        element.appendChild(submittedWeek);

        Element deadlineWeek = document.createElement("deadlineWeek");
        deadlineWeek.setTextContent(String.valueOf(entity.getHomework().getDeadlineWeek()));
        element.appendChild(deadlineWeek);

        Element feedback = document.createElement("feedback");
        feedback.setTextContent(String.valueOf(entity.getFeedback()));
        element.appendChild(feedback);

        return element;
    }

}
