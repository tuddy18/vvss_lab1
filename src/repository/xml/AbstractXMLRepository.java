package repository.xml;

import domain.HasID;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import repository.InMemoryRepository;
import validation.ValidationException;
import validation.Validator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;


public abstract class AbstractXMLRepository<ID, E extends HasID<ID> >  extends InMemoryRepository<ID, E> {
    String fileName;
    public AbstractXMLRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
    }

    protected void readFromFile() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();

            Document document = builder.parse(new File(fileName));

            document.getDocumentElement().normalize();

            Element root = document.getDocumentElement();
            NodeList childNodes = root.getChildNodes();
            for (int i = 0; i < childNodes.getLength(); i++) {
                Node child = childNodes.item(i);
                if (child.getNodeType() == Node.ELEMENT_NODE) {
                    E entity = extractEntity((Element) child);
                    super.save(entity);
                }
            }
        }
        catch (Exception e) {
            //.printStackTrace();
        }
    }

    protected void writeToFile() {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.newDocument();

            String[] res = fileName.split("\\.");
            res = res[res.length - 2].split("/");
            Element root = document.createElement(res[res.length - 1]);
            document.appendChild(root);
            findAll().forEach(e -> {
                Element childNode = insertEntity(document, e);
                root.appendChild(childNode);
            });

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.transform(new DOMSource(document), new StreamResult(fileName));
        }catch (Exception ex) {
        }
    }
    abstract E extractEntity(Object e);

    abstract Element insertEntity(Document document, E entity);

    @Override
    public E save(E entity) throws ValidationException {
        E res = super.save(entity);
        if(res == null)
            writeToFile();
        return res;
    }

    @Override
    public E delete(ID id) {
        E res = super.delete(id);
        if(res != null)
            writeToFile();
        return res;
    }

    @Override
    public E update(E entity) throws ValidationException {
        E res = super.update(entity);
        if(res != null)
            writeToFile();
        return res;
    }

}
