package repository.xml;

import domain.Homework;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import repository.xml.AbstractXMLRepository;
import validation.Validator;

import java.util.function.Function;

public class HomeworkXMLRepository extends AbstractXMLRepository<Integer, Homework> {
    public HomeworkXMLRepository(String fileName, Validator<Homework> validator) {
        super(fileName, validator);
        readFromFile();
    }

    @Override
    Element insertEntity(Document document, Homework h) {
        Element element = document.createElement("homework");
        element.setAttribute("id", h.getID().toString());

        Element description = document.createElement("description");
        description.setTextContent(h.getDescription());
        element.appendChild(description);

        Element assignedWeek = document.createElement("assignedWeek");
        assignedWeek.setTextContent(h.getAssignedWeek().toString());
        element.appendChild(assignedWeek);

        Element deadlineWeek = document.createElement("deadlineWeek");
        deadlineWeek.setTextContent(String.valueOf(h.getDeadlineWeek()));
        element.appendChild(deadlineWeek);

        return element;
    }

    @Override
    Homework extractEntity(Object e) {
        Element homework = (Element)e;
        Function<String, String> getElement = x -> homework.getElementsByTagName(x).item(0).getTextContent();

        String id = homework.getAttribute("id");
        String description = getElement.apply("description");
        String assignedWeek = getElement.apply("assignedWeek");
        String deadlineWeek = getElement.apply("deadlineWeek");

        return new Homework(Integer.valueOf(id), description, Byte.valueOf(assignedWeek), Byte.valueOf(deadlineWeek));
    }
}
