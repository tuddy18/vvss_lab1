package repository.txt;

import domain.Homework;
import repository.txt.AbstractFileRepository;
import validation.Validator;

public class HomeworkRepository extends AbstractFileRepository<Integer, Homework> {


    public HomeworkRepository(String fileName, Validator<Homework> validator) {
        super(fileName, validator);
    }

    @Override
    Homework extractEntity(Object e) {
        String line = (String)e;
        String[] attr = line.split(",");
        return new Homework(Integer.parseInt(attr[0]), attr[1], Byte.parseByte(attr[2]), Byte.parseByte(attr[3]));
    }
}
