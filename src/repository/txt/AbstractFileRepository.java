package repository.txt;

import domain.HasID;
import repository.InMemoryRepository;
import validation.ValidationException;
import validation.Validator;

import java.io.*;

public abstract class AbstractFileRepository<ID, E extends HasID<ID>> extends InMemoryRepository<ID,E> {
    String fileName;

    public AbstractFileRepository(String fileName, Validator<E> validator) {
        super(validator);
        this.fileName = fileName;
        readFromFile();

    }

    private void readFromFile() {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while((line = reader.readLine())!= null){
                E tmp = extractEntity(line);
                super.save(tmp);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeToFile(E entity, boolean append){
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(fileName,append))){
            bw.write(entity.toString());
            bw.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void writeAllToFile() {
        boolean append = false;
        if(entities.values().isEmpty()) {
            try {
                new PrintWriter(fileName).close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        for (E el: entities.values()) {
            writeToFile(el, append);
            append = true;
        }
    }

    @Override
    public E save(E entity){
        E res = super.save(entity);
        if(res == null) {
            writeToFile(entity, true);
        }
        return res;
    }

    @Override
    public E delete(ID id) {
        E res = super.delete(id);
        if(res != null) {
            writeAllToFile();
        }
        return  res;
    }

    @Override
    public E update(E entity) throws ValidationException {
        E res = super.update(entity);
        if(res != null) {
            writeAllToFile();
        }
        return res;
    }

    abstract E extractEntity(Object e);

}
