package repository.txt;

import domain.Student;
import repository.txt.AbstractFileRepository;
import validation.Validator;

public class StudentRepository extends AbstractFileRepository<Integer, Student> {

    public StudentRepository(String fileName, Validator<Student> validator) {
        super(fileName, validator);
    }

    @Override
    Student extractEntity(Object e) {
        String line = (String) e;
        String[] attr = line.split(",");
        return new Student(Integer.parseInt(attr[0]), attr[1], Short.parseShort(attr[2]), attr[3], attr[4]);
    }
}
