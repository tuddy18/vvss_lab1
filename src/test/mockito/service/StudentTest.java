package test.mockito.service;


import domain.Student;

import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import org.mockito.runners.MockitoJUnitRunner;
import repository.CrudRepository;
import service.StudentService;
import validation.ExistentStudentException;
import validation.InexistentStudentException;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class StudentTest {

    @InjectMocks
    private StudentService studentService;

    @Mock
    private CrudRepository<Integer, Student> studentRepository;

    @Mock
    private CrudRepository<Integer, Student> deletedStudentRepository;

    @Test
    public void addStudent_Fail_ExistentStudentException() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(null).when(deletedStudentRepository).save(any(Student.class));
        doReturn(student).when(studentRepository).save(any(Student.class));

        try {
            studentService.addStudent(10, "a", (short) 1, "a@a", "teacher");
            fail("Should not reach this point.");
        } catch (ExistentStudentException e) {
            assertEquals("Existent student!", e.getMessage());
        }
    }

    @Test
    public void addStudent_Success() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(student).when(deletedStudentRepository).save(any(Student.class));
        doReturn(null).when(studentRepository).save(any(Student.class));

        try {
            studentService.addStudent(10, "a", (short) 1, "a@a", "teacher");
        } catch (ExistentStudentException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void updateStudent_Fail_InexistentStudentException() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(null).when(studentRepository).findOne(any(Integer.class));

        try {
            studentService.updateStudent(10, (short) 1, "a@a", "teacher");
            fail("Should not reach this point.");
        } catch (InexistentStudentException e) {
            assertEquals("Inexistent student!", e.getMessage());
        }
    }

    @Test
    public void updateStudent_Success() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(student).when(studentRepository).findOne(any(Integer.class));
        doReturn(student).when(studentRepository).update(any(Student.class));

        try {
            studentService.updateStudent(10, (short) 1, "a@a", "teacher");
        } catch (InexistentStudentException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void deleteStudent_Fail_InexistentStudentException() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(null).when(studentRepository).delete(any(Integer.class));

        try {
            studentService.deleteStudent(10);
            fail("Should not reach this point.");
        } catch (InexistentStudentException e) {
            assertEquals("Inexistent student!", e.getMessage());
        }
    }

    @Test
    public void deleteStudent_Success() {
        Student student = new Student();
        student.setName("ioan grozea");

        doReturn(student).when(studentRepository).delete(any(Integer.class));

        try {
            studentService.deleteStudent(10);
        } catch (InexistentStudentException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void getAllStudents_Success() {
        Student student = new Student();
        student.setName("ioan grozea");
        Student student2 = new Student();
        student.setName("chuck");

        List<Student> students = new ArrayList<>();
        students.add(student);
        students.add(student2);

        doReturn(students).when(studentRepository).findAll();


        try {
            Iterable<Student> list = studentService.getAllStudents();
            assertEquals(students, list);
        } catch (InexistentStudentException e) {
            fail("Should not reach this point.");
        }
    }

}
