package test.mockito.service;


import domain.Homework;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import repository.CrudRepository;
import service.HomeworkService;
import validation.ExistentHomeworkException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class HomeworkTest {

    @InjectMocks
    private HomeworkService homeworkService;

    @Mock
    private CrudRepository<Integer, Homework> homeworkRepository;

    @Test
    public void addHomework_Fail_ExistentHomeworkException() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(homework).when(homeworkRepository).save(any(Homework.class));

        try {
            homeworkService.addHomework(10, "a", (byte) 0, (byte) 0);
            fail("Should not reach this point.");
        } catch (ExistentHomeworkException e) {
            assertEquals("Existent homework!", e.getMessage());
        }
    }

    @Test
    public void addHomework_Success() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //scenariu normal de test
            homeworkService.addHomework(10, "a", (byte) 4, (byte) 5);

        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void addHomework_Fail_Deadline_Earlier() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //deadline mai devreme
            homeworkService.addHomework(10, "a", (byte) 5, (byte) 4);
            fail("Should not reach this point.");
        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void addHomework_Fail_Deadline_Too_Big() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //deadline prea mare
            homeworkService.addHomework(10, "a", (byte) 14, (byte) 15);
            fail("Should not reach this point");
        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void addHomework_Fail_Assignment_And_Deadline_Too_Big() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //assignment si deadline week prea mari
            homeworkService.addHomework(10, "a", (byte) 15, (byte) 16);
            fail("Should not reach this point.");

        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void addHomework_Same_Week() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //tema in aceeasi saptamana 14-14
            homeworkService.addHomework(10, "a",(byte) 14, (byte) 14);
            fail("Should not reach this point.");

        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }

    @Test
    public void addHomework_Existent_Name() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //scenariu normal de test
            homeworkService.addHomework(10, "a", (byte) 4, (byte) 5);

            //tema cu nume la fel pot fii aaugate
            homeworkService.addHomework(10, "a",(byte) 14, (byte) 14);
            fail("Should not reach this point.");

        } catch (ExistentHomeworkException e) {
            fail("Should not reach this point.");
        }
    }


    @Test
    public void addHomework_Fail_NumberFormatException() {
        Homework homework = new Homework();
        homework.setDescription("ioan grozea");

        doReturn(null).when(homeworkRepository).save(any(Homework.class));

        try {
            //invalid input - text
            homeworkService.addHomework(10, "a", Byte.valueOf("goenirge"), (byte) 15);
            fail("Should not reach this point.");
        } catch (NumberFormatException e) {

        }
    }

}
