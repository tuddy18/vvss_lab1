package test.mockito.service;

import domain.Grade;
import domain.Homework;
import domain.MotivatedAbsence;
import domain.Student;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.doReturn;

import org.mockito.runners.MockitoJUnitRunner;

import repository.CrudRepository;
import service.GradeService;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@RunWith(MockitoJUnitRunner.class)
public class GradeTest {

    @InjectMocks
    private GradeService gradeService;

    @Mock
    private CrudRepository<Integer, Student> studentCrudRepository;

    @Mock
    private CrudRepository<Integer, Homework> homeworkCrudRepository;

    @Mock
    private Map<String, CrudRepository<Integer, Grade>> gradeRepos = new HashMap<>();

    @Mock
    private CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository;

    @Mock
    private CrudRepository<Integer, Grade> gradeCrudRepository;


    @Test
    public void addGrade_Success() {
        byte b = 55;
        Student student = new Student();
        student.setName("ioan grozea");

        Homework homework = new Homework();
        homework.setDescription("some random grade mock");

        Grade grade = new Grade();
        grade.setValue(4.0);

        doReturn(student).when(studentCrudRepository).findOne(any(Integer.class));
        doReturn(homework).when(homeworkCrudRepository).findOne(any(Integer.class));
        doReturn(gradeCrudRepository).when(gradeRepos).get(any(String.class));
        doReturn(grade).when(gradeCrudRepository).save(any(Grade.class));

        try {

            gradeService.addGrade(2,2, 4.0, "yup");

        } catch (IOException e) {
            fail("Should not reach this point.");
        }

    }

}
