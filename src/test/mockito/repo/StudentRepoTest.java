package test.mockito.repo;
import domain.Student;
import org.junit.*;
import repository.db.AbstractDatabaseRepository;
import repository.db.StudentDatabaseRepository;
import validation.StudentValidator;
import validation.Validator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class StudentRepoTest {
    AbstractDatabaseRepository<Integer,Student> repository;

    @Before
    public void initialize() {
        try {

            Validator<Student> studentValidator = new StudentValidator();
            repository = new StudentDatabaseRepository("students", studentValidator, false);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void addStudent() {
        Student student = new Student();
        student.setName("chuck");

        Student student1 = repository.save(student);

        Assert.assertEquals(student, student1);
    }

    @Test
    public void updateStudent() {
        Student student = new Student();
        student.setName("chuck");

        student = repository.save(student);
        student.setName("ioan grozea");

        Student student1 = repository.update(student);

        Assert.assertEquals(student, student1);
    }

    @Test
    public void deleteStudent() {
        Student student = new Student();
        student.setName("chuck");

        student = repository.save(student);

        Student student1 = repository.delete(student.getID());

        Assert.assertEquals(student, student1);
    }

    @Test
    public void readStudent() {
        Student student = new Student();
        student.setName("chuck");

        Student student1 = new Student();
        student1.setName("ioan grozea");

        List<Student> students = new ArrayList<>();
        students.add(student);
        students.add(student1);

        repository.save(student);
        repository.save(student1);


        Assert.assertEquals(students, repository.findAll());
    }

}
