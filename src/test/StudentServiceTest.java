package test;

import domain.Student;
import junit.framework.TestCase;
import repository.CrudRepository;
import repository.InMemoryRepository;
import service.StudentService;
import validation.*;


public class StudentServiceTest extends TestCase {
    StudentService studentService;
    public void setUp() throws Exception {
        Validator<Student> studentValidator = new StudentValidator();
        CrudRepository<Integer, Student> studentCrudRepository = new InMemoryRepository<>(studentValidator);
        CrudRepository<Integer, Student> deletedStudentCrudRepository = new InMemoryRepository<>(studentValidator);
        studentCrudRepository.save(new Student(1,"Ionel", (short)211, "ioir1111@scs.ubbcluj.ro",
                "Vasile Vasile"));
        studentCrudRepository.save(new Student(2,"Marcel", (short)211, "mair2222@scs.ubbcluj.ro",
                "Ion Ion"));
        studentCrudRepository.save(new Student(3,"Gigel", (short)113, "gimr3333@scs.ubbcluj.ro",
                "Vasile Vasile"));
        deletedStudentCrudRepository.save(new Student(4,"Alex", (short)211, "alir4444@scs.ubbcluj.ro",
                "Vasile Vasile"));
        deletedStudentCrudRepository.save(new Student(5,"Mircea", (short)315, "miir5555@scs.ubbcluj.ro",
                "Ion Ion"));
        deletedStudentCrudRepository.save(new Student(6,"George", (short)217, "geir6666@scs.ubbcluj.ro",
                "Vasile Vasile"));
        studentService = new StudentService(studentCrudRepository, deletedStudentCrudRepository);
    }

    public void tearDown() throws Exception {
    }

    public void testAddStudent() {
        studentService.addStudent(88, "Vasilica", (short)224, "vair8888@scs.ubbcluj.ro", "John");
        int l = 0;
        for(Student s : studentService.getAllStudents()) {
            l++;
        }
        assertEquals(4 , l);
        try {
            studentService.addStudent(888, "Vasilica", (short)228, "vair8888@scs.ubbcluj.ro", "John");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.addStudent(888, "Vasilica99", (short)224, "vair8888@scs.ubbcluj.ro", "John");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.addStudent(888, "Vasilica", (short)224, "vair8888@ubbcluj.ro", "John");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.addStudent(888, "Vasilica", (short)224, "vair8888@scs.ubbcluj.ro", "");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.addStudent(88, "Vasilica", (short)224, "vair8888@scs.ubbcluj.ro", "John");
            assert (false);
        }
        catch (ExistentStudentException e) {
            assert (true);
        }

        try {
            studentService.addStudent(1, "Vasilica", (short)224, "vair8888@scs.ubbcluj.ro", "John");
            assert (false);
        }
        catch (ExistentStudentException e) {
            assert (true);
        }
    }

    public void testUpdateStudent() {
        studentService.updateStudent(1, (short)227, "vsir1234@scs.ubbcluj.ro", "Ionut");
        Student s = studentService.getStudent(1);
        assertEquals("vsir1234@scs.ubbcluj.ro", s.getEmail());
        assertEquals("Ionut", s.getLabTeacherName());
        assertEquals(227, (int)s.getGroup());
        try {
            studentService.updateStudent(11, (short)227, "vsir1234@scs.ubbcluj.ro", "Ionut");
            assert (false);
        }
        catch (InexistentStudentException e) {
            assert (true);
        }

        try {
            studentService.updateStudent(1, (short)228, "vair8888@scs.ubbcluj.ro", "John");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.updateStudent(1, (short)224, "vair8888@ubbcluj.ro", "John");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            studentService.updateStudent(1, (short)224, "vair8888@scs.ubbcluj.ro", "");
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }
    }

    public void testGetStudent() {
        Student s = studentService.getStudent(1);
        assertEquals("ioir1111@scs.ubbcluj.ro", s.getEmail());
        assertEquals("Ionel", s.getName());
        assertEquals("Vasile Vasile", s.getLabTeacherName());
        assertEquals(211, (int)s.getGroup());
        assertEquals(1, (int)s.getID());
    }

    public void testGetDeletedStudent() {
        Student s = studentService.getDeletedStudent(4);
        assertEquals("alir4444@scs.ubbcluj.ro", s.getEmail());
        assertEquals("Alex", s.getName());
        assertEquals("Vasile Vasile", s.getLabTeacherName());
        assertEquals(211, (int)s.getGroup());
        assertEquals(4, (int)s.getID());
    }

    public void testDeleteStudent() {
        studentService.deleteStudent(1);
        int l = 0;
        for(Student s : studentService.getAllStudents()) {
            l++;
        }
        assertEquals( 2, l);
        l = 0;
        for(Student s : studentService.getAllDeletedStudents()) {
            l++;
        }
        assertEquals(4 , l);

        try {
            studentService.deleteStudent(1);
            assert (false);
        }
        catch (InexistentStudentException e) {
            assert (true);
        }
    }

    public void testPermanentlyDeleteStudent() {
        studentService.permanentlyDeleteStudent(4);
        int l = 0;
        for(Student s : studentService.getAllDeletedStudents()) {
            l++;
        }
        assertEquals(2 , l);

        try {
            studentService.permanentlyDeleteStudent(4);
            assert (false);
        }
        catch (InexistentStudentException e) {
            assert (true);
        }
    }

    public void testRecoverStudent() {
        studentService.recoverStudent(4);
        int l = 0;
        for(Student s : studentService.getAllStudents()) {
            l++;
        }
        assertEquals( 4, l);
        l = 0;
        for(Student s : studentService.getAllDeletedStudents()) {
            l++;
        }
        assertEquals(2 , l);

        try {
            studentService.recoverStudent(4);
            assert (false);
        }
        catch (InexistentStudentException e) {
            assert (true);
        }
    }

    public void testGetAllStudents() {
        int l = 0;
        for(Student s : studentService.getAllStudents()) {
            l++;
        }
        assertEquals(l , 3);
    }

    public void testGetAllDeletedStudents() {
        int l = 0;
        for(Student s : studentService.getAllDeletedStudents()) {
            l++;
        }
        assertEquals(l , 3);
    }
}