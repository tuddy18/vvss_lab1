package test;

import domain.Homework;
import domain.MotivatedAbsence;
import domain.Student;
import junit.framework.TestCase;
import repository.CrudRepository;
import repository.InMemoryRepository;
import service.GradeService;
import service.WeekManager;
import validation.CannotAddGradeException;
import validation.HomeworkValidator;
import validation.MotivatedAbsenceValidator;
import validation.StudentValidator;

import java.io.IOException;

public class GradeServiceTest extends TestCase {
    private GradeService gradeService;
    byte oldWeek;

    @Override
    protected void setUp() throws Exception {
        CrudRepository<Integer, Student> studentCrudRepository = new InMemoryRepository<>(new StudentValidator());
        studentCrudRepository.save(new Student(1,"Ionel", (short)211, "ioir1111@scs.ubbcluj.ro",
                "Vasile Vasile"));
        studentCrudRepository.save(new Student(2,"Marcel", (short)211, "mair2222@scs.ubbcluj.ro",
                "Ion Ion"));
        studentCrudRepository.save(new Student(3,"Gigel", (short)113, "gimr3333@scs.ubbcluj.ro",
                "Vasile Vasile"));

        CrudRepository<Integer, Homework> homeworkCrudRepository = new InMemoryRepository<>(new HomeworkValidator());
        homeworkCrudRepository.save(new Homework(1,"Description", (byte)3,(byte)5));
        homeworkCrudRepository.save(new Homework(2,"Description2", (byte)1,(byte)2));

        CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository = new InMemoryRepository<>(
                new MotivatedAbsenceValidator());
        Student s = studentCrudRepository.findOne(1);
        motivatedAbsenceCrudRepository.save(new MotivatedAbsence(1, studentCrudRepository.findOne(1), (byte)5,
                "gfdgfd"));

        gradeService = new GradeService(studentCrudRepository, homeworkCrudRepository, motivatedAbsenceCrudRepository);
        oldWeek = WeekManager.getInstance().getCurrentWeek();
        WeekManager.getInstance().setCurrentWeek((byte)6);

    }

    @Override
    protected void tearDown() throws Exception {
        WeekManager.getInstance().setCurrentWeek(oldWeek);
    }

    public void testAddGrade() throws IOException {
        gradeService.addGrade(1,1,8.5, "sd");
        assertEquals(8.5, gradeService.getGrade(1,1).getValue());
        gradeService.addGrade(2,1,7.5,"sfdfds");
        assertEquals(5.0,gradeService.getGrade(2,1).getValue());

        try {
            gradeService.addGrade(3,2,10.0,"fgfd");
            assert (false);
        }
        catch (CannotAddGradeException e) {
            assert (true);
        }
    }
}
