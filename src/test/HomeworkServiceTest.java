package test;

import domain.Homework;
import junit.framework.TestCase;
import repository.CrudRepository;
import repository.InMemoryRepository;
import service.HomeworkService;
import service.WeekManager;
import validation.*;

public class HomeworkServiceTest extends TestCase {
    private HomeworkService homeworkService;

    public void setUp() throws Exception {
        Validator<Homework> homeworkValidator = new HomeworkValidator();
        CrudRepository<Integer, Homework> homeworkCrudRepository = new InMemoryRepository<>(homeworkValidator);
        homeworkCrudRepository.save(new Homework(2,"Description", (byte)3,(byte)5));
        homeworkService = new HomeworkService(homeworkCrudRepository);
    }

    public void tearDown() throws Exception {
    }

    public void testAddHomework() {
        homeworkService.addHomework(1,"Tema smechera la MAP", (byte)1, (byte)2);
        int l = 0;
        for(Homework s : homeworkService.getAllHomework()) {
            l++;
        }
        assertEquals(2 , l);
        try {
            homeworkService.addHomework(1,"Tema smechera la MAP", (byte)1, (byte)2);
            assert (false);
        }
        catch (ExistentHomeworkException e) {
            assert (true);
        }

        try {
            homeworkService.addHomework(2,"Tema smechera la MAP", (byte)2, (byte)2);
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            homeworkService.addHomework(2,"", (byte)2, (byte)15);
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            homeworkService.addHomework(-2,"Tema smechera la MAP", (byte)2, (byte)5);
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }

        try {
            homeworkService.addHomework(2,"", (byte)2, (byte)3);
            assert (false);
        }
        catch (ValidationException e) {
            assert (true);
        }
    }

    public void testModifyHomeworkDeadline() {
        homeworkService.modifyHomeworkDeadline(2, (byte)5);
        Homework h = homeworkService.getHomework(2);
        assertEquals(5, (int)h.getDeadlineWeek());
        try {
            homeworkService.modifyHomeworkDeadline(2, (byte)3);
            assert (false);
        }
        catch (InvalidChangeException e) {
            assert (true);
        }
        try {
            WeekManager.getInstance().setCurrentWeek((byte)6);
        } catch (InvalidWeekException e) {
            e.printStackTrace();
        }
        try {
            homeworkService.modifyHomeworkDeadline(2, (byte)8);
            assert (false);
        }
        catch (InvalidChangeException e) {
            assert (true);
        }
    }
}