package main;

import domain.User;
import gui.*;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import repository.*;
import repository.db.UserDatabaseRepository;
import service.*;
import service.WeekManager;
import validation.*;

import java.sql.*;

public class GUIMain extends Application {
    private LoginWindowController loginWindowController;
    @Override
    public void start(Stage primaryStage) throws Exception {
        try {
            performLogin(primaryStage);
        }
        catch (SQLException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, "Server error. Contact the server admin!");
        }
        catch (Exception e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, "Unexpected error! Call the bastard who wrote " +
                    "the code!");
        }
    }

    private void performLogin(Stage primaryStage) throws Exception {
        Validator<User> userValidator = new UserValidator();
        CrudRepository<String, User> userCrudRepository =
                new UserDatabaseRepository("Users", userValidator, true);
        UserService userService = new UserService(userCrudRepository);
        FXMLLoader loginWindowLoader = new FXMLLoader();
        loginWindowLoader.setLocation(getClass().getResource(
                "/resources/views/loginWindow.fxml"));
        BorderPane rootLayout = loginWindowLoader.load();
        loginWindowController = loginWindowLoader.getController();
        Scene loginScene = new Scene(rootLayout);
        primaryStage.setScene(loginScene);
        loginWindowController.setPrimaryStage(primaryStage);
        loginWindowController.setUserService(userService);
        primaryStage.initStyle(StageStyle.UNDECORATED);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
