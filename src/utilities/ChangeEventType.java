package utilities;

public enum ChangeEventType {
    ADD,UPDATE,DELETE, RECOVER, PERMANENTLY_DELETE;
}
