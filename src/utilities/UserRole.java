package utilities;

public enum UserRole {
    ADMIN, SECRETARY, TEACHER, SUPERUSER
}
