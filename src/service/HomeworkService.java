package service;

import domain.HasID;
import domain.Homework;
import repository.CrudRepository;
import utilities.*;
import validation.ExistentHomeworkException;
import validation.InexistentHomeworkException;
import validation.InvalidChangeException;
import validation.ValidationException;


import java.util.ArrayList;
import java.util.List;

public class HomeworkService implements Service, Observable<EntityChangeEvent<HasID>> {

    private CrudRepository<Integer, Homework> homeworkRepository;
    private List<Observer<EntityChangeEvent<HasID>>> observers = new ArrayList<>();

    public HomeworkService(CrudRepository<Integer, Homework> homeworkRepository) {
        this.homeworkRepository = homeworkRepository;

    }

    /**
     * Adds a new student
     *
     * @param ID           - id of the homework
     * @param description  - short description of the homework
     * @param assignedWeek - the week when the homework was assigned
     * @param deadlineWeek - the week when the homework is due
     * @throws ValidationException       if the attributes are invalid
     * @throws ExistentHomeworkException if there is a homework with the specified id
     */
    public void addHomework(Integer ID, String description, Byte assignedWeek, Byte deadlineWeek) {
        Homework h = new Homework(ID, description, assignedWeek, deadlineWeek), h2;
        h2 = homeworkRepository.save(h);
        if (h2 != null)
            throw new ExistentHomeworkException("Existent homework!");
        notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, h));
    }

    public void addHomework(String description, Byte assignedWeek, Byte deadlineWeek) {
        Homework h = new Homework((int)getAllHomework().spliterator().getExactSizeIfKnown() + 1, description, assignedWeek, deadlineWeek), h2;
        h2 = homeworkRepository.save(h);
        if (h2 != null)
            throw new ExistentHomeworkException("Existent homework!");
        notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, h));
    }

    /**
     * Modifies the deadline of a homework
     *
     * @param ID      - id of the homework
     * @param newWeek - the new due week for the homework with the given id, newWeek must be between 1 and 14
     * @throws InexistentHomeworkException if there is no homework with this id
     * @throws InvalidChangeException      if the due week cannot be changed
     */
    public void modifyHomeworkDeadline(Integer ID, Byte newWeek) {
        Homework h = homeworkRepository.findOne(ID);
        if (h == null)
            throw new InexistentHomeworkException("Inexistent homework!");
        if (h.getDeadlineWeek() >= WeekManager.getInstance().getCurrentWeek() && h.getDeadlineWeek() <= newWeek) {
            h.setDeadlineWeek(newWeek);
            homeworkRepository.update(h);
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.UPDATE, h));
        }
        else
            throw new InvalidChangeException("Cannot change previously finished homework or shorten the deadline!");
    }

    /**
     * @return all the homework
     */
    public Iterable<Homework> getAllHomework() {
        return homeworkRepository.findAll();
    }

    /**
     * @param ID      - id of the homework
     * @return the homework with the specified id
     */
    public Homework getHomework(Integer ID) {
        return homeworkRepository.findOne(ID);
    }

    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.remove(observers.indexOf(e));
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observers.forEach(x->x.update(t));
    }
}
