package service;

import domain.*;
import repository.CrudRepository;
import repository.db.GradeDatabaseRepository;
import repository.db.StudentDatabaseRepository;
import repository.xml.GradeXMLRepository;
import utilities.*;
import utilities.Observable;
import utilities.Observer;
import validation.*;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GradeService implements Service, Observable<EntityChangeEvent<HasID>> {

    private CrudRepository<Integer, Student> studentCrudRepository;
    private CrudRepository<Integer, Homework> homeworkCrudRepository;
    private CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository;
    private Map<String, CrudRepository<Integer, Grade>> gradeRepos;
    private List<Observer<EntityChangeEvent<HasID>>> observers = new ArrayList<>();


    public GradeService(CrudRepository<Integer, Student> studentCrudRepository,
                        CrudRepository<Integer, Homework> homeworkCrudRepository,
                        CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository) {
        this.studentCrudRepository = studentCrudRepository;
        this.homeworkCrudRepository = homeworkCrudRepository;
        this.motivatedAbsenceCrudRepository = motivatedAbsenceCrudRepository;
        gradeRepos = new HashMap<>();
    }

    /**
     * Adds a grade of the given value, automatically applying a penalty if necessary
     * @param studentID - id of the student
     * @param homeworkID - id of the homework
     * @param value - value of the mark
     * @param feedback - feedback for the given grade
     * @throws ExistentGradeException if there is a grade for this homework assigned to this student
     * @throws CannotAddGradeException if the homework cannot be graded because the delay is longer than 2 weeks and
     * there are no motivated absences for this period
     */
    public void addGrade(Integer studentID, Integer homeworkID, Double value, String feedback) throws IOException {
        Student s = studentCrudRepository.findOne(studentID);
        //Homework h = homeworkCrudRepository.findOne(homeworkID);
        Validator<Grade> gradeValidator = new GradeValidator();
        CrudRepository<Integer, Grade> gradeCrudRepository = gradeRepos.get("s" + s.getID() + ".xml");
        if(gradeCrudRepository == null)
        {
            gradeCrudRepository =
                    //new GradeXMLRepository("./src/resources/grades/s" + s.getStudentID() + ".xml", gradeValidator,
                    //        studentCrudRepository, homeworkCrudRepository);
                    new GradeDatabaseRepository("s" + s.getID(), gradeValidator, studentCrudRepository,
                            homeworkCrudRepository, true);
            gradeRepos.put("s" + s.getID(), gradeCrudRepository);
        }
        value = gradeAfterPenalty(value, studentID, homeworkID);
        if(value == -1)
            throw new CannotAddGradeException("Cannot add a grade! This homework is graded by default with 1!");
        Grade r = gradeCrudRepository.save(new Grade(value, studentCrudRepository.findOne(studentID),
                homeworkCrudRepository.findOne(homeworkID), WeekManager.getInstance().getCurrentWeek(), feedback));
        if(r != null)
            throw new ExistentGradeException("Existent grade for this homework!");
        else
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
    }

    public Grade getGrade(Integer studentID, Integer homeworkID) {
        CrudRepository<Integer, Grade> gradeCrudRepository = gradeRepos.get("s" + studentID);
        Validator<Grade> gradeValidator = new GradeValidator();
        if(gradeCrudRepository == null) {
            try {
                gradeCrudRepository =
                        new GradeDatabaseRepository("s" + studentID, gradeValidator, studentCrudRepository, homeworkCrudRepository,
                                false);
                gradeRepos.put("s" + studentID, gradeCrudRepository);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            Grade g = gradeCrudRepository.findOne(Integer.valueOf(studentID.toString() + homeworkID));
            if(g == null)
                throw new InexistentGradeException("No grade for this student at this homework!");
            return g;
        }
        catch (NullPointerException e) {
            throw new InexistentGradeException("No grade for this student at this homework!");
        }
    }

    public void deleteGrade(Integer studentID, Integer homeworkID) {
        Grade g = getGrade(studentID, homeworkID);
        g = gradeRepos.get("s" + studentID).delete(g.getID());
        if(g == null)
            throw new InexistentGradeException("No grade for this student at this homework!");
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.DELETE, g));
    }

    public void updateGrade(Integer studentID, Integer homeworkID, Double grade) {
        Grade g = getGrade(studentID, homeworkID);
        g.setValue(grade);
        g = gradeRepos.get("s" + studentID).update(g);
        if(g == null)
            throw new InexistentGradeException("No grade for this student at this homework!");
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, g));
    }

    /**
     * Computes the penalty for a grade
     * @param value - initial value of the grade
     * @param studentID - id of the student
     * @param homeworkID - id of the homework
     * @return the grade after the possible penalty is applied
     */
    private Double gradeAfterPenalty(Double value, Integer studentID, Integer homeworkID) {
        Iterable<MotivatedAbsence> motivatedAbsences = motivatedAbsenceCrudRepository.findAll();
        List<MotivatedAbsence> motivatedAbsenceList = StreamSupport.stream(motivatedAbsences.spliterator(), false)
                .collect(Collectors.toList());
        Byte deadline = homeworkCrudRepository.findOne(homeworkID).getDeadlineWeek();
        int absences = WeekManager.getInstance().getCurrentWeek() - deadline;
        Predicate<MotivatedAbsence> absenceFilter = x -> x.getStudent().getID().equals(studentID);
        List<MotivatedAbsence> filtered = motivatedAbsenceList.stream()
                .filter(absenceFilter.and(x -> x.getWeek() >= deadline &&
                        x.getWeek() <= WeekManager.getInstance().getCurrentWeek()))
                .collect(Collectors.toList());
        absences -= filtered.size();
        if(absences > 2)
            return -1d;
        else
            return Math.max(value - Math.max(absences, 0) * 2.5, 1);
    }

    public Double getPossiblePenalty(Integer studentId, Integer homeworkId){
        Double gradeAfterPenalty = gradeAfterPenalty(10.0, studentId, homeworkId);
        return gradeAfterPenalty == -1 ? -1 : 10 - gradeAfterPenalty;
    }

    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.remove(observers.indexOf(e));
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observers.forEach(x->x.update(t));
    }

    private Iterable<Grade> retrieveDataFromXML() {
        File dir = new File("./src/resources/grades");
        List<Grade> allGrades = new ArrayList<>();
        File[] files = dir.listFiles();
        if(files == null)
            return allGrades;
        Validator<Grade> gradeValidator = new GradeValidator();
        for(File f : files) {
            CrudRepository<Integer, Grade> gradeCrudRepository = gradeRepos.get(f.getName());
            if(gradeCrudRepository == null) {
                gradeCrudRepository =
                        new GradeXMLRepository("./src/resources/grades/"+f.getName(), gradeValidator,
                                studentCrudRepository, homeworkCrudRepository);
                gradeRepos.put(f.getName(), gradeCrudRepository);
            }
            allGrades.addAll(StreamSupport.stream(gradeCrudRepository.findAll().spliterator(), false)
                    .collect(Collectors.toList()));
        }
        return allGrades;
    }

    private Iterable<Grade> retrieveDataFromDB() {
        List<Grade> allGrades = new ArrayList<>();
        Validator<Grade> gradeValidator = new GradeValidator();
        ((StudentDatabaseRepository) studentCrudRepository).getAllDatabaseTableNames("s%").forEach(t -> {
            if(!t.matches("s[0-9]*"))
                return;
            if(studentCrudRepository.findOne(Integer.valueOf(t.substring(1))) == null)
                return;
            CrudRepository<Integer, Grade> gradeCrudRepository = gradeRepos.get(t);
            if(gradeCrudRepository == null) {
                try {
                    gradeCrudRepository =
                            new GradeDatabaseRepository(t, gradeValidator, studentCrudRepository, homeworkCrudRepository,
                                    false);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                allGrades.addAll(StreamSupport.stream(gradeCrudRepository.findAll().spliterator(), false)
                        .collect(Collectors.toList()));
            }
            catch (NullPointerException e) {

            }
        });
        return allGrades;
    }

    public Iterable<Grade> getAllGrades() {
        return retrieveDataFromDB();
    }

    public Map<Double, Long> getGradesDistributionForHomework(int homeworkID) {
        Map<Double, Integer> gradeDist = new HashMap<>();
        Iterable<Grade> grades = getAllGrades();
        grades = StreamSupport.stream(grades.spliterator(), false)
                .filter(g -> g.getHomework().getID().equals(homeworkID))
                .collect(Collectors.toList());
        return StreamSupport.stream(grades.spliterator(), false)
                .collect(Collectors.groupingBy(Grade::getValue, Collectors.counting()));
    }


    public Map<Student, Double> getTopNStudents(Integer n) {
        Iterable<Grade> grades = getAllGrades();
        Integer totalWeeks = StreamSupport.stream(homeworkCrudRepository.findAll().spliterator(), false)
                .collect(Collectors.summingInt(h -> h.getDeadlineWeek() - h.getAssignedWeek()));

        return StreamSupport.stream(grades.spliterator(), false)
                .collect(Collectors.groupingBy(Grade::getStudent,
                        Collectors.averagingDouble(g -> ((Grade)g).getValue() *
                                (((Grade)g).getHomework().getDeadlineWeek() - ((Grade)g).getHomework().getAssignedWeek()) / totalWeeks)))
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(n)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue,
                        LinkedHashMap::new));
    }

    public Map<Homework, Double> getAverageGradeForEachHomework() {
        Iterable<Grade> grades = getAllGrades();
        return StreamSupport.stream(grades.spliterator(), false)
                .collect(Collectors.groupingBy(Grade::getHomework, Collectors.averagingDouble(Grade::getValue)));
    }

}
