package service;

import domain.Break;
import domain.HasID;
import repository.CrudRepository;
import utilities.ChangeEventType;
import utilities.EntityChangeEvent;
import utilities.Observable;
import utilities.Observer;
import validation.InexistentBreakException;
import validation.InvalidWeekException;
import validation.OverlappingBreaksException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public final class WeekManager implements Observable<EntityChangeEvent<HasID>> {
    private Byte currentWeek = 1;
    private DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy");
    private Integer totalBreakDays ;
    private CrudRepository<Integer, Break> breakCrudRepository;
    private List<Observer> observerList = new ArrayList<>();

    public void setBreakCrudRepository(CrudRepository<Integer, Break> breakCrudRepository) {
        this.breakCrudRepository = breakCrudRepository;
    }

    private static final WeekManager INSTANCE = new WeekManager();

    private WeekManager() {
    }
    public static WeekManager getInstance() {
        return INSTANCE;
    }

    public Byte dateToWeek(LocalDate date) {
        totalBreakDays = 0;
        int allDays = Math.max((int)ChronoUnit.DAYS.between(getUniversityYearBegining(), date), 0);
        if(allDays == 0)
            return -1;
        getAllBreaks().forEach(b -> {if(b.getFinish().isBefore(date) || b.getFinish().isEqual(date))
            totalBreakDays += b.getDurationInDays();
        else if(b.getStart().isBefore(date) && b.getFinish().isAfter(date))
            totalBreakDays += (int)ChronoUnit.DAYS.between(b.getStart(), date) + 1;}) ;
        return (byte)((allDays - totalBreakDays) / 7 + 1);
    }

    public Byte getCurrentWeek() {
        LocalDate now = LocalDate.now();
        //return dateToWeek(now);
        return 7;
    }


    public void addBreak(LocalDate start, LocalDate finish, String description) {
        List<Break> breaks = StreamSupport.stream(getAllBreaks().spliterator(), false)
                .collect(Collectors.toList());
        Boolean overlap = breaks.stream().anyMatch(b -> b.getStart().isBefore(finish) && start.isBefore(b.getFinish()));
        if(overlap)
            throw new OverlappingBreaksException("Breaks must not overlap!");
        Break b = new Break(breaks.size() + 1, start, finish, description);
        breakCrudRepository.save(b);
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.ADD, b));
    }

    public void updateBreak(Integer breakID, LocalDate start, LocalDate finish, String description) {
        Break b = breakCrudRepository.update(new Break(breakID, start, finish, description));
        if(b == null)
            throw new InexistentBreakException("No brake with this ID!");
        else
            notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, b));
    }

    public LocalDate getUniversityYearBegining() {
        Break b = breakCrudRepository.findOne(0);
        if(b == null)
            return null;
        return b.getStart();
    }

    public void setUniversityYearBegining(LocalDate start) {
        breakCrudRepository.update(new Break(0, start, start,"UNI YEAR BEGIN"));
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, null));
    }

    public void setCurrentWeek(Byte currentWeek) throws InvalidWeekException {
        if(currentWeek < (byte) 1 || currentWeek > (byte) 14)
            throw new InvalidWeekException("Invalid week! Value should be between 1 and 14!");
        this.currentWeek = currentWeek;
    }

    public Iterable<Break> getAllBreaks() {
        List<Break> breaks = StreamSupport.stream(breakCrudRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        return breaks.subList(1, breaks.size());
    }

    public void deleteAllBreaks() {
        getAllBreaks().forEach(b -> {if(b.getID() != 0) breakCrudRepository.delete(b.getID());});
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.DELETE, null));
    }

    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observerList.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observerList.remove(e);
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observerList.forEach(o -> o.update(t));
    }


    public void deleteBreak(Integer breakID) {
        Break r = breakCrudRepository.delete(breakID);
        if(r == null)
            throw new InexistentBreakException("There is no break with this id!");
    }
}
