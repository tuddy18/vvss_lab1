package service;

import domain.MotivatedAbsence;
import domain.Student;
import repository.CrudRepository;
import validation.ExistentMotivatedAbsenceException;
import validation.InexistentMotivatedAbsenceException;

import java.util.Iterator;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MotivatedAbsenceService implements Service {

    private CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository;
    private CrudRepository<Integer, Student> studentCrudRepository;

    public MotivatedAbsenceService(CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository,
                                   CrudRepository<Integer, Student> studentCrudRepository) {
        this.motivatedAbsenceCrudRepository = motivatedAbsenceCrudRepository;
        this.studentCrudRepository = studentCrudRepository;
    }

    /**
     * Adds a motivated absence
     * @param studentID - id of the student
     * @param week - week of the absence
     * @param reason - reason of the absence
     * @throws ExistentMotivatedAbsenceException if the absence already exists
     */
    public void addMotivatedAbsence(Integer studentID, Byte week, String reason) {
        MotivatedAbsence a = new MotivatedAbsence(Integer.valueOf(week.toString() + studentID.toString()),
                studentCrudRepository.findOne(studentID), week, reason);
        MotivatedAbsence r = motivatedAbsenceCrudRepository.save(a);
        if(r != null)
            throw new ExistentMotivatedAbsenceException("Existent absence for this student!");
    }

    public void updateMotivatedAbsence(Integer studentID, Byte week, String reason) {
        MotivatedAbsence a = new MotivatedAbsence(Integer.valueOf(week.toString() + studentID.toString()),
                studentCrudRepository.findOne(studentID), week, reason);
        MotivatedAbsence r = motivatedAbsenceCrudRepository.update(a);
        if(r == null)
            throw new InexistentMotivatedAbsenceException("No absence for this student and this week!");
    }

    public void deleteMotivatedAbsence(Integer studentID, Byte week) {
        MotivatedAbsence r = motivatedAbsenceCrudRepository.delete(Integer.valueOf(week.toString() + studentID.toString()));
        if(r == null)
            throw new InexistentMotivatedAbsenceException("No absence for this student and this week!");
    }

    public MotivatedAbsence getAbsence(Integer studentID, Byte week) {
        return motivatedAbsenceCrudRepository.findOne(Integer.valueOf(week.toString() + studentID.toString()));
    }

    public Iterable<MotivatedAbsence> getAbsencesForStudent(Integer studentID) {
        Iterable<MotivatedAbsence> motivatedAbsences = motivatedAbsenceCrudRepository.findAll();
        if(motivatedAbsences == null)
            return null;
        return StreamSupport.stream(motivatedAbsences.spliterator(), false)
                .filter(a -> a.getStudent().getID().equals(studentID))
                .collect(Collectors.toList());
    }
}
