package service;

import domain.HasID;
import domain.User;
import repository.CrudRepository;
import utilities.*;
import validation.ExistentUserException;
import validation.InexistentUserException;

import javax.security.auth.login.LoginException;
import java.util.ArrayList;
import java.util.List;

public class UserService implements Service, Observable<EntityChangeEvent<HasID>> {
    private CrudRepository<String, User> userCrudRepository;
    private List<Observer<EntityChangeEvent<HasID>>> observers = new ArrayList<>();

    public UserService(CrudRepository<String, User> userCrudRepository) {
        this.userCrudRepository = userCrudRepository;
    }

    public User performLogin(String username, String password) throws LoginException {
        User user = userCrudRepository.findOne(username);
        if(user == null)
            throw new LoginException("There is no user with this username!");
        if(!user.getPassword().equals(password))
           throw new LoginException("Incorrect password for the given user!");
        return user;
    }

    public void addUser(String username, String password, UserRole userRole, String name) {
        User user = new User(username, password, userRole, name);
        User user2 = userCrudRepository.save(user);
        if(user2 != null)
            throw new ExistentUserException("There already is a user with this username!");
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.ADD, user));
    }

    public void deleteUser(String username) {
        User u = userCrudRepository.delete(username);
        if(u == null)
            throw new InexistentUserException("There is no user with this username!");
        else
            notifyObservers(new EntityChangeEvent<>(ChangeEventType.DELETE, u));
    }

    public void updateUser(String username, String password) {
        User user = userCrudRepository.findOne(username);
        if(user == null)
            throw new InexistentUserException("There is no user with this username!");
        user.setPassword(password);
        userCrudRepository.update(user);
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, user));
    }

    public void updateUser(String username, String password, UserRole userRole, String name) {
        User user = userCrudRepository.findOne(username);
        if(user == null)
            throw new InexistentUserException("There is no user with this username!");
        user.setPassword(password);
        user.setUserRole(userRole);
        user.setName(name);
        userCrudRepository.update(user);
        notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, user));
    }

    public Iterable<User> getAllUsers() {
        return userCrudRepository.findAll();
    }

    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.remove(observers.indexOf(e));
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observers.forEach(x->x.update(t));
    }


}
