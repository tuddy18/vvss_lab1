package service;

import domain.HasID;
import domain.UserFeedbackTemplate;
import repository.CrudRepository;
import utilities.ChangeEventType;
import utilities.EntityChangeEvent;
import utilities.Observable;
import utilities.Observer;
import validation.InexistentUserFeedbackTemplate;

import java.util.ArrayList;
import java.util.List;

public class UserFeedbackTemplateService implements Service, Observable<EntityChangeEvent<HasID>> {
    private CrudRepository<String, UserFeedbackTemplate> userFeedbackTemplateCrudRepository;
    private List<Observer<EntityChangeEvent<HasID>>> observers = new ArrayList<>();

    public UserFeedbackTemplateService(CrudRepository<String, UserFeedbackTemplate> userFeedbackTemplateCrudRepository) {
        this.userFeedbackTemplateCrudRepository = userFeedbackTemplateCrudRepository;
    }

    public void addOrUpdateUserFeedbackTemplate(String username, String feedbackTemplate) {
        UserFeedbackTemplate userFeedbackTemplate = new UserFeedbackTemplate(username, feedbackTemplate);
        UserFeedbackTemplate u = userFeedbackTemplateCrudRepository.update(userFeedbackTemplate);
        if(u == null) {
            u = userFeedbackTemplateCrudRepository.save(userFeedbackTemplate);
            notifyObservers(new EntityChangeEvent<>(ChangeEventType.ADD, u));
        }
        else
            notifyObservers(new EntityChangeEvent<>(ChangeEventType.UPDATE, u));
    }

    public void deleteFeedbackTemplate(String username) {
        UserFeedbackTemplate u = userFeedbackTemplateCrudRepository.delete(username);
        if(u == null)
            throw new InexistentUserFeedbackTemplate("No feedback template for this username!");
        else
            notifyObservers(new EntityChangeEvent<>(ChangeEventType.DELETE, u));
    }

    public UserFeedbackTemplate getUserFeedbackTemplate(String username) {
        UserFeedbackTemplate u = userFeedbackTemplateCrudRepository.findOne(username);
        if(u == null)
            throw new InexistentUserFeedbackTemplate("No feedback template for this username!");
        else
            return u;
    }

    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.remove(observers.indexOf(e));
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observers.forEach(x->x.update(t));
    }
}
