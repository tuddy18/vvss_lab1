package service;

import domain.HasID;
import domain.Student;
import repository.CrudRepository;
import utilities.ChangeEventType;
import utilities.EntityChangeEvent;
import utilities.Observable;
import utilities.Observer;
import validation.ExistentStudentException;
import validation.InexistentStudentException;

import java.util.ArrayList;
import java.util.List;

public class StudentService implements Service, Observable<EntityChangeEvent<HasID>> {
    private CrudRepository<Integer, Student> studentRepository;
    private CrudRepository<Integer, Student> deletedStudentRepository;
    private List<Observer<EntityChangeEvent<HasID>>> observers=new ArrayList<>();

    public StudentService(CrudRepository<Integer, Student> repository,
                          CrudRepository<Integer, Student> deletedStudentRepository) {
        this.studentRepository = repository;
        this.deletedStudentRepository = deletedStudentRepository;
    }

    /**
     * Adds a student
     *
     * @param ID          - the id of the student
     * @param name        - the name of the student
     * @param group       - the group of the student
     * @param email       - the email of the student
     * @param teacherName - the teacher of the student
     * @throws ExistentStudentException if there is already a student with the given id or there might be such a student
     *                                  in the repository with the deleted students
     */
    public void addStudent(Integer ID, String name, Short group, String email, String teacherName) {
        Student s;
        s = deletedStudentRepository.findOne(ID);
        if (s != null)
            throw new ExistentStudentException("There was a student like this in the past! Consider reintroducing him " +
                    "or delete that entry permanently!");
        s = new Student(ID, name, group, email, teacherName);
        s = studentRepository.save(s);
        if (s != null)
            throw new ExistentStudentException("Existent student!");
        else
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
    }

    /**
     * @param ID          - the id of the student
     * @param group       - the group of the student
     * @param email       - the email of the student
     * @param teacherName - the teacher of the student
     * @throws InexistentStudentException if there is no student with the given id
     */
    public void updateStudent(Integer ID, Short group, String email, String teacherName) {
        Student tmp = studentRepository.findOne(ID);
        if (tmp == null)
            throw new InexistentStudentException("Inexistent student!");
        Student s = new Student(ID, tmp.getName(), group, email, teacherName);
        s = studentRepository.update(s);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
    }

    /**
     * @param ID - the id of the student
     * @return the student with the given id
     */
    public Student getStudent(Integer ID) {
        Student s = studentRepository.findOne(ID);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else
            return s;
    }

    /**
     * @param ID - the id of the deleted student
     * @return the deleted student with the given id
     */
    public Student getDeletedStudent(Integer ID) {
        Student s = deletedStudentRepository.findOne(ID);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else
            return s;
    }

    /**
     * @param ID - the id of the student
     */
    public void deleteStudent(Integer ID) {
        Student s = studentRepository.delete(ID);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else {
            deletedStudentRepository.save(s);
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
        }
    }

    /**
     * Permanently deletes a student
     *
     * @param ID - the id of the deleted student
     */
    public void permanentlyDeleteStudent(Integer ID) {
        Student s = deletedStudentRepository.delete(ID);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
    }

    /**
     * Recovers a student from the deleted student repository by adding him back to repository
     *
     * @param ID - the id of the deleted student
     */
    public void recoverStudent(Integer ID) {
        Student s = deletedStudentRepository.delete(ID);
        if (s == null)
            throw new InexistentStudentException("Inexistent student!");
        else {
            Student r = studentRepository.save(s);
            notifyObservers(new EntityChangeEvent<HasID>(ChangeEventType.ADD, s));
        }
    }

    /**
     * @return all the currently studying students
     */
    public Iterable<Student> getAllStudents() {
        return studentRepository.findAll();
    }

    /**
     * @return all the currently not studying students
     */
    public Iterable<Student> getAllDeletedStudents() {
        return deletedStudentRepository.findAll();
    }


    @Override
    public void addObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<EntityChangeEvent<HasID>> e) {
        observers.remove(observers.indexOf(e));
    }

    @Override
    public void notifyObservers(EntityChangeEvent<HasID> t) {
        observers.forEach(x->x.update(t));
    }
}
