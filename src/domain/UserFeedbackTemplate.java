package domain;

public class UserFeedbackTemplate implements HasID<String> {
    private String username;
    private String feedbackTemplate;

    public UserFeedbackTemplate(String username, String feedbackTemplate) {
        this.username = username;
        this.feedbackTemplate = feedbackTemplate;
    }

    @Override
    public String getID() {
        return username;
    }

    @Override
    public void setID(String s) {
        username = s;
    }

    public String getFeedbackTemplate() {
        return feedbackTemplate;
    }

    public void setFeedbackTemplate(String feedbackTemplate) {
        this.feedbackTemplate = feedbackTemplate;
    }
}
