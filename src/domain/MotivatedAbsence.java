package domain;

public class MotivatedAbsence implements HasID<Integer> {
    private Integer id;
    private Byte week;
    private Student s;
    private String reason;

    public MotivatedAbsence(Integer id, Student s, Byte week, String reason) {
        this.id = id;
        this.s = s;
        this.week = week;
        this.reason = reason;
    }


    public Byte getWeek() {
        return week;
    }

    public Student getStudent() {
        return s;
    }

    public String getReason() {
        return reason;
    }

    @Override
    public Integer getID() {
        return id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MotivatedAbsence)
            return ((MotivatedAbsence) obj).getID().equals(id);
        return false;
    }

    @Override
    public void setID(Integer integer) {
        id = integer;
    }
}
