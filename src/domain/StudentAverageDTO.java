package domain;

public class StudentAverageDTO {
    private Integer id;
    private String name;
    private Short group;
    private Double average;

    public Short getGroup() {
        return group;
    }

    public void setGroup(Short group) {
        this.group = group;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public StudentAverageDTO(Integer id, String name, Short group, Double average) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.average = average;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
