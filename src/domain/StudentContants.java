package domain;

public class StudentContants {
    public static int MIN_SPECIALIZATION_NR = 1, MAX_SPECIALIZATION_NR = 5;
    public static int MIN_YEAR_NR = 1, MAX_YEAR_NR = 3;
    public static int MIN_GROUP_NR = 1, MAX_GROUP_NR = 7;
}
