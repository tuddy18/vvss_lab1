package domain;

import java.util.Objects;

public class Homework implements HasID<Integer> {

    private Integer id;
    private String description;
    private Byte assignedWeek;
    private Byte deadlineWeek;

    public Homework() {
        this.id = 0;
        this.description = "";
        this.assignedWeek = (byte) 0;
        this.deadlineWeek = (byte) 0;
    }


    public Homework(Integer id, String description, Byte assignedWeek, Byte deadlineWeek) {
        this.id = id;
        this.description = description;
        this.assignedWeek = assignedWeek;
        this.deadlineWeek = deadlineWeek;
    }


    @Override
    public String toString() {
        return id.toString() + "," + description + "," + assignedWeek.toString() + "," + deadlineWeek.toString();
    }

    @Override
    public Integer getID() {
        return id;
    }

    @Override
    public void setID(Integer integer) {
        id = integer;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAssignedWeek(Byte assignedWeek) {
        this.assignedWeek = assignedWeek;
    }

    public void setDeadlineWeek(Byte deadlineWeek) {
        this.deadlineWeek = deadlineWeek;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Homework)
            return ((Homework)obj).getID().equals(id);
        else  return false;
    }


    public String getDescription() {
        return description;
    }

    public Byte getAssignedWeek() {
        return assignedWeek;
    }

    public Byte getDeadlineWeek() {
        return deadlineWeek;
    }
}
