package domain;

import java.util.Objects;

public class Grade implements HasID<Integer> {
    private Integer ID;
    private Student s;
    private Homework h;
    private Double value;
    private Byte submittedWeek;
    private String feedback;

    public Grade() {}

    public Grade(Double value, Student s, Homework h, Byte submittedWeek) {
        this.s = s;
        this.h = h;
        this.value = value;
        this.submittedWeek = submittedWeek;
        this.ID = Integer.valueOf(s.getID().toString() + h.getID());
    }

    public Grade(Double value, Student s, Homework h, Byte submittedWeek, String feedback) {
        this.s = s;
        this.h = h;
        this.value = value;
        this.submittedWeek = submittedWeek;
        this.feedback = feedback;
        this.ID = Integer.valueOf(s.getID().toString() + h.getID());
    }


    @Override
    public int hashCode() {
        return Objects.hash(s.getID(),h.getID());
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Grade) {
            return ((Grade) obj).s.equals(s) && ((Grade) obj).h.equals(h);
        }
        else return false;
    }


    @Override
    public Integer getID() {
        return ID;
    }

    @Override
    public void setID(Integer homework) {
        ID = homework;
    }

    public Homework getHomework() {
        return h;
    }

    public Student getStudent() {
        return s;
    }

    public Byte getSubmittedWeek() {
        return submittedWeek;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getFeedback() {
        return feedback;
    }
}
