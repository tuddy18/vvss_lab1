package domain;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Break implements HasID<Integer> {
    private Integer ID;
    private LocalDate start;
    private LocalDate finish;
    private String description;

    public Break(Integer ID, LocalDate start, LocalDate finish, String description) {
        this.start = start;
        this.finish = finish;
        this.ID = ID;
        this.description = description;
    }

    public LocalDate getStart() {
        return start;
    }

    public void setStart(LocalDate start) {
        this.start = start;
    }

    public LocalDate getFinish() {
        return finish;
    }

    public void setFinish(LocalDate finish) {
        this.finish = finish;
    }

    public int getDurationInDays() {
        return (int) ChronoUnit.DAYS.between(start, finish);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Integer getID() {
        return ID;
    }

    @Override
    public void setID(Integer integer) {
        this.ID = integer;
    }

    public String getDescription() {
        return description;
    }
}
