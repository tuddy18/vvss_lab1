package domain;

import java.util.Objects;

public class Student implements HasID<Integer> {

    private Integer id;
    private String name;
    private Short group;
    private String labTeacherName;
    private String email;

    public Student() {
        this.id = 0;
        this.name = "";
        this.group = (short) 0;
        this.labTeacherName = "";
        this.email = "";
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Student(Integer id, String name, Short group, String email, String labTeacherName) {
        this.id = id;
        this.name = name;
        this.group = group;
        this.email = email;
        this.labTeacherName = labTeacherName;
    }

    @Override
    public String toString() {
        return id.toString() + "," + name + "," + group.toString() + "," + email + "," + labTeacherName;
    }


    /**
     * @return the ID of the Student
     */
    @Override
    public Integer getID() {
        return id;
    }

    /**
     * @param id - the ID of the Student
     */
    @Override
    public void setID(Integer id) {
        this.id = id;
    }

    /**
     * @return the ID of the Student
     */
    public String getEmail() {
        return email;
    }

    /**
     * @return the ID of the Student
     */
    public String getName() {
        return name;
    }

    /**
     * @return the ID of the Student
     */
    public Short getGroup() {
        return group;
    }

    /**
     * @return the ID of the Student
     */
    public String getLabTeacherName() {
        return labTeacherName;
    }

    /**
     * @param name - the name of the Student
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param group - the group of the Student
     */
    public void setGroup(Short group) {
        this.group = group;
    }

    /**
     * @param labTeacherName - the teacher of the Student
     */
    public void setLabTeacherName(String labTeacherName) {
        this.labTeacherName = labTeacherName;
    }

    /**
     * @param email - the email of the Student
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @param obj - Object instance
     * @return true if the given object is equal to the current, false otherwise
     */
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Student)
            return ((Student) obj).getID().equals(id);
        return false;
    }

}
