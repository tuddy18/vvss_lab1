package domain;

import utilities.UserRole;

public class User implements HasID<String> {
    private String username;
    private String password;
    private UserRole userRole;
    private String name;

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof User)
            return username.equals(((User) obj).username);
        return false;
    }

    public User(String username, String password, UserRole userRole, String name) {
        this.username = username;
        this.password = password;
        this.userRole = userRole;
        this.name = name;
    }

    @Override
    public String getID() {
        return username;
    }

    @Override
    public void setID(String s) {
        username = s;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
