package domain;

public class GradeDTO {
    private Integer studentID;
    private Integer homeworkID;
    private String student;
    private Short group;
    private String homework;
    private Double grade;
    private String teacher;

    public GradeDTO(Integer studentID, Integer homeworkID, String student, Short group, String homework, Double grade, String teacher) {
        this.studentID = studentID;
        this.homeworkID = homeworkID;
        this.student = student;
        this.group = group;
        this.homework = homework;
        this.grade = grade;
        this.teacher = teacher;
    }

    public Integer getStudentID() {
        return studentID;
    }

    public void setStudentID(Integer studentID) {
        this.studentID = studentID;
    }

    public Integer getHomeworkID() {
        return homeworkID;
    }

    public void setHomeworkID(Integer homeworkID) {
        this.homeworkID = homeworkID;
    }

    public String getStudent() {
        return student;
    }

    public void setStudent(String student) {
        this.student = student;
    }

    public Short getGroup() {
        return group;
    }

    public void setGroup(Short group) {
        this.group = group;
    }

    public String getHomework() {
        return homework;
    }

    public void setHomework(String homework) {
        this.homework = homework;
    }

    public Double getGrade() {
        return grade;
    }

    public void setGrade(Double grade) {
        this.grade = grade;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }
}
