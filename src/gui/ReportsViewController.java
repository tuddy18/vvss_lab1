package gui;

import domain.HasID;
import domain.HomeworkDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.print.PrinterJob;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.util.StringConverter;
import service.GradeService;
import service.HomeworkService;
import service.StudentService;
import utilities.EntityChangeEvent;
import utilities.Observer;
import utilities.ReportType;
import validation.ValidationException;

import java.io.IOException;

public class ReportsViewController implements Observer<EntityChangeEvent<HasID>> {
    private GradeService gradeService;
    private HomeworkService homeworkService;
    private StudentService studentService;
    private BorderPane reportsView;
    private AnchorPane gradesDistributionForHomeworkView;
    private AnchorPane topNStudentsView;
    private AnchorPane averageGradeForEachHomeworkView;
    private GradesDistributionForHomeworkViewController gradesDistributionForHomeworkViewController;
    private TopNStudentsViewController topNStudentsViewController;
    private  AverageGradeForEachHomeworkViewController averageGradeForEachHomeworkViewController;
    private ObservableList<ReportType> reportTypes = FXCollections.observableArrayList();
    @FXML
    ComboBox selectReportComboBox;


    private void initializeComponents() {
        selectReportComboBox.setConverter(new StringConverter<ReportType>() {

            @Override
            public String toString(ReportType object) {
                return object.toString().replace('_', ' ');
            }

            @Override
            public ReportType fromString(String string) {
                return ReportType.valueOf(string.replace(' ', '_'));
            }
        });
        reportTypes.addAll(ReportType.values());
        selectReportComboBox.setItems(reportTypes);
    }

    private void initializeGradesDistributionForHomeworkView() throws IOException {
        FXMLLoader gradesDistributionForHomeworkViewLoader = new FXMLLoader();
        gradesDistributionForHomeworkViewLoader.setLocation(getClass().getResource(
                "/resources/views/gradesDistributionForHomeworkView.fxml"));
        AnchorPane rootGradesDistributionForHomework = gradesDistributionForHomeworkViewLoader.load();
        gradesDistributionForHomeworkViewController = gradesDistributionForHomeworkViewLoader.getController();
        gradesDistributionForHomeworkViewController.setHomeworkService(homeworkService);
        gradesDistributionForHomeworkViewController.setGradeService(gradeService);
        gradesDistributionForHomeworkViewController.init();
        setGradesDistributionForHomeworkView(rootGradesDistributionForHomework);
    }

    private void initializeTopNStudentsView() throws IOException {
        FXMLLoader topNStudentsViewLoader = new FXMLLoader();
        topNStudentsViewLoader.setLocation(getClass().getResource(
                "/resources/views/topNStudentsView.fxml"));
        AnchorPane rootTopNStudents = topNStudentsViewLoader.load();
        topNStudentsViewController = topNStudentsViewLoader.getController();
        topNStudentsViewController.setGradeService(gradeService);
        topNStudentsViewController.init();
        setTopNStudentsView(rootTopNStudents);
    }

    private void initializeAverageGradeForEachHomeworkView() throws IOException {
        FXMLLoader averageGradeForEachHomeworkViewLoader = new FXMLLoader();
        averageGradeForEachHomeworkViewLoader.setLocation(getClass().getResource(
                "/resources/views/averageGradeForEachHomeworkView.fxml"));
        AnchorPane rootAverageGradeForEachHomework = averageGradeForEachHomeworkViewLoader.load();
        averageGradeForEachHomeworkViewController = averageGradeForEachHomeworkViewLoader.getController();
        averageGradeForEachHomeworkViewController.setHomeworkService(homeworkService);
        averageGradeForEachHomeworkViewController.setGradeService(gradeService);
        averageGradeForEachHomeworkViewController.init();
        setAverageGradeForEachHomeworkView(rootAverageGradeForEachHomework);
    }

    private void initializeView() throws IOException {
        initializeGradesDistributionForHomeworkView();
        initializeTopNStudentsView();
        initializeAverageGradeForEachHomeworkView();

    }

    private void saveToPDFAction() {
        PrinterJob job = PrinterJob.createPrinterJob();
        if(selectReportComboBox.getSelectionModel().getSelectedItem() == null)
            return;
        //job.showPrintDialog(null);
        if (job != null)
            switch ((ReportType) selectReportComboBox.getSelectionModel().getSelectedItem()) {
                case TOP_N_STUDENTS:
                    job.getJobSettings().setJobName("Top " +
                            topNStudentsViewController.topNStudentsTableView.getItems().size() + " students");
                    if (job.printPage(topNStudentsViewController.topNStudentsTableView))
                        job.endJob();
                    break;
                case AVERAGE_GRADE_FOR_EACH_HOMEWORK:
                    job.getJobSettings().setJobName("Homework average grades");
                    if (job.printPage(averageGradeForEachHomeworkViewController.averageGradeForEachHomeworkBarChart))
                        job.endJob();
                    break;
                case GRADE_DISTRIBUTION_FOR_HOMEWORK:
                    job.getJobSettings().setJobName("Grade distribution " +
                            (((HomeworkDTO) gradesDistributionForHomeworkViewController.selectHomeworkComboBox
                                    .getSelectionModel().getSelectedItem())).getDescription());
                    if (job.printPage(gradesDistributionForHomeworkViewController.gradeDistributionBarChart))
                        job.endJob();
                    break;
                default:
                    break;
            }
        else
            throw new ValidationException("Cannot save this file!");
    }

    public void setGradeService(GradeService gradeService) throws IOException {
        this.gradeService = gradeService;
    }

    public void setGradesDistributionForHomeworkView(AnchorPane gradesDistributionForHomeworkView) {
        this.gradesDistributionForHomeworkView = gradesDistributionForHomeworkView;
    }

    public void setTopNStudentsView(AnchorPane topNStudentsView) {
        this.topNStudentsView = topNStudentsView;
    }

    public void setAverageGradeForEachHomeworkView(AnchorPane averageGradeForEachHomeworkView) {
        this.averageGradeForEachHomeworkView = averageGradeForEachHomeworkView;
    }

    public void setReportsView(BorderPane reportsView) {
        this.reportsView = reportsView;
    }

    public void setHomeworkService(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void handleSelectReportComboBoxOnAction(ActionEvent actionEvent) {
        switch ((ReportType)selectReportComboBox.getSelectionModel().getSelectedItem()) {
            case TOP_N_STUDENTS:
                reportsView.setCenter(topNStudentsView);
                break;
            case AVERAGE_GRADE_FOR_EACH_HOMEWORK:
                reportsView.setCenter(averageGradeForEachHomeworkView);
                break;
            case GRADE_DISTRIBUTION_FOR_HOMEWORK:
                reportsView.setCenter(gradesDistributionForHomeworkView);
                break;
            default:
                break;
        }
    }


    @Override
    public void update(EntityChangeEvent<HasID> changeEvent) {
        gradesDistributionForHomeworkViewController.update(changeEvent);
    }

    public void handleSaveToPDFOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        try {
            saveToPDFAction();
        }
        catch (SecurityException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, "You don't have the required permissions to do this " +
                    "operation!");
        }
        catch (ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }

    public void init() throws IOException {
        initializeComponents();
        initializeView();
    }
}
