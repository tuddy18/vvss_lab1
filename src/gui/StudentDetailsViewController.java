package gui;

import domain.Student;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.StudentService;
import utilities.ChangeEventType;
import validation.ExistentStudentException;
import validation.InexistentStudentException;
import validation.ValidationException;

import java.io.IOException;

public class StudentDetailsViewController {
    private Stage studentDetailsStage;
    private StudentService studentService;
    private ChangeEventType eventType;
    private Student student;
private
    @FXML
    TextField idTextField;
    @FXML
    TextField nameTextField;
    @FXML
    TextField groupTextField;
    @FXML
    TextField emailTextField;
    @FXML
    TextField labTeacherTextField;
    @FXML
    Label idErrorLabel;
    @FXML
    Label nameErrorLabel;
    @FXML
    Label groupErrorLabel;
    @FXML
    Label emailErrorLabel;
    @FXML
    Label labTeacherErrorLabel;

    private void initializeView() {
        if(student == null)
            return;
        idTextField.setText(student.getID().toString());
        nameTextField.setText(student.getName());
        groupTextField.setText(student.getGroup().toString());
        emailTextField.setText(student.getEmail());
        labTeacherTextField.setText(student.getLabTeacherName());
    }

    private void submitAction() {
        switch (eventType) {
            case ADD:
                studentService.addStudent(Integer.valueOf(idTextField.getText()), nameTextField.getText(),
                        Short.valueOf(groupTextField.getText()), emailTextField.getText(), labTeacherTextField.getText());
                studentDetailsStage.close();
                break;
            case UPDATE:
                studentService.updateStudent(Integer.valueOf(idTextField.getText()),
                        Short.valueOf(groupTextField.getText()), emailTextField.getText(), labTeacherTextField.getText());
                studentDetailsStage.close();
                break;
        }
    }

    public void setEventType(ChangeEventType eventType) {
        this.eventType = eventType;
        if(eventType == ChangeEventType.UPDATE) {
            idTextField.setEditable(false);
            nameTextField.setEditable(false);
            //submitButton.setText("Update");
        }
        else if(eventType == ChangeEventType.ADD) {
            idTextField.setEditable(true);
            nameTextField.setEditable(true);
            //submitButton.setText("Add");
        }
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void setStudentDetailsStage(Stage studentDetailsStage) {
        this.studentDetailsStage = studentDetailsStage;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public void handleEnterKeyPressed(KeyEvent event) throws IOException {
        handleEnterKeyTyped(event);
    }

    public void handleEnterKeyTyped(KeyEvent actionEvent) throws IOException {
        ((BorderPane)actionEvent.getSource()).requestFocus();
        if(!validateSubmitStudent())
            return;
        try {
            submitAction();
        }
        catch (InexistentStudentException | ExistentStudentException | ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
        catch (NumberFormatException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, "Invalid number entered!");
        }
    }

    private boolean validateSubmitStudent() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        if (idTextField.getText().length() == 0) {
            idErrorLabel.setText("Invalid id!");
            idTextField.setStyle(errorStyle);
            errors = true;
        }
        if (nameTextField.getText().length() == 0) {
            nameErrorLabel.setText("Invalid name!");
            nameTextField.setStyle(errorStyle);
            errors = true;
        }
        if (groupTextField.getText().length() == 0) {
            groupErrorLabel.setText("Invalid group!");
            groupTextField.setStyle(errorStyle);
            errors = true;
        }
        if (emailTextField.getText().length() == 0 ||
                !emailTextField.getText().matches("[^@]+@[^\\.]+\\..+")) {
            emailErrorLabel.setText("Invalid email!");
            emailTextField.setStyle(errorStyle);
            errors = true;
        }
        if (labTeacherTextField.getText().length() == 0) {
            labTeacherErrorLabel.setText("Invalid teacher name!");
            labTeacherTextField.setStyle(errorStyle);
            errors = true;
        }
        return !errors;
    }

    public void handleMinimizeMouseClicked(MouseEvent mouseEvent) {
        studentDetailsStage.setIconified(true);
    }

    public void handleExitMouseClicked(MouseEvent mouseEvent) {
        studentDetailsStage.close();
    }

    public void handleTextFieldKeyTyped(KeyEvent keyEvent) {
        TextField field = ((TextField)keyEvent.getSource());
        field.setStyle(null);
        if(field.equals(nameTextField))
            nameErrorLabel.setText(null);
        else if(field.equals(groupTextField))
            groupErrorLabel.setText(null);
        else if(field.equals(emailTextField))
            emailErrorLabel.setText(null);
        else if(field.equals(idTextField))
            idErrorLabel.setText(null);
        else labTeacherErrorLabel.setText(null);
    }

    public void init() {
        initializeView();
    }
}
