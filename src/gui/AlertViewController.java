package gui;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


import java.io.File;
import java.io.IOException;

public class AlertViewController {
    private Stage stage;
    private Alert.AlertType alertType;
    private ButtonType clicked;
    private double xOffset = 0;
    private double yOffset = 0;
    @FXML
    Label messageLabel;
    @FXML
    ImageView alertImageView;
    @FXML
    ImageView okImageView;
    @FXML
    ImageView cancelImageView;

    private static AlertViewController createAlertWindow() throws IOException {
        Stage stage = new Stage();
        stage.initStyle(StageStyle.UNDECORATED);
        FXMLLoader alertViewLoader = new FXMLLoader();
        alertViewLoader.setLocation(AlertViewController.class.getResource("../resources/views/alertView.fxml"));
        BorderPane rootAlert = alertViewLoader.load();
        AlertViewController controller = alertViewLoader.getController();
        rootAlert.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                controller.xOffset = event.getSceneX();
                controller.yOffset = event.getSceneY();
            }
        });
        rootAlert.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - controller.xOffset);
                stage.setY(event.getScreenY() - controller.yOffset);
            }
        });
        stage.setScene(new Scene(rootAlert));
        stage.initModality(Modality.APPLICATION_MODAL);
        controller.stage = stage;
        return controller;
    }

    public static ButtonType showAlert(Alert.AlertType alertType, String message) throws IOException {
        AlertViewController controller = AlertViewController.createAlertWindow();
        File file;
        Image image = null;
        switch (alertType) {
            case ERROR:
                file = new File("src/resources/images/error.png");
                image = new Image(file.toURI().toString());
                controller.cancelImageView.setVisible(false);
                break;
            case INFORMATION:
                file = new File("src/resources/images/warning.png");
                image = new Image(file.toURI().toString());
                controller.cancelImageView.setVisible(false);
                break;
            case WARNING:
                file = new File("src/resources/images/warning.png");
                image = new Image(file.toURI().toString());
                controller.cancelImageView.setVisible(true);
                break;
            default:
                break;
        }
        controller.alertImageView.setImage(image);
        controller.messageLabel.setText(message);
        controller.stage.showAndWait();
        return controller.clicked;
    }

    public void handleOkImageViewOnMouseClicked(MouseEvent mouseEvent) {
        clicked = ButtonType.OK;
        stage.close();
    }

    public void handleCancelImageViewOnMouseClicked(MouseEvent mouseEvent) {
        clicked = ButtonType.CANCEL;
        stage.close();
    }
}
