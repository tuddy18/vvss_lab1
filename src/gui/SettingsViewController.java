package gui;

import domain.Break;
import domain.HasID;
import domain.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.UserService;
import service.WeekManager;
import utilities.*;
import validation.InexistentUserException;
import validation.OverlappingBreaksException;
import validation.ValidationException;

import java.io.File;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SettingsViewController implements Observer<EntityChangeEvent<HasID>> {
    private User user;
    private UserService userService;
    private UserDetailsViewController userDetailsViewController;
    private ObservableList<User> users = FXCollections.observableArrayList();
    private List<Break> breakList;
    private Integer currentBreak = 1;
    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    TableView usersTableView;
    @FXML
    TableColumn usernameTableViewColumn;
    @FXML
    TableColumn roleTableViewColumn;
    @FXML
    TableColumn nameTableViewColumn;
    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField oldPasswordField;
    @FXML
    PasswordField newPasswordField;
    @FXML
    PasswordField confirmNewPasswordField;
    @FXML
    Tab myAccountTab;
    @FXML
    Tab accountsTab;
    @FXML
    Tab timeSettingsTab;
    @FXML
    DatePicker academicYearDatePicker;
    @FXML
    DatePicker breakBeginDatePicker;
    @FXML
    DatePicker breakEndDatePicker;
    @FXML
    Label breakSelectionLabel;
    @FXML
    TextField descriptionTextField;
    @FXML
    CheckBox addNewVacationCheckBox;
    @FXML
    ImageView leftArrowImageView;
    @FXML
    ImageView rightArrowImageView;
    @FXML
    ImageView addUpdateImageView;
    @FXML
    ImageView deleteImageView;
    @FXML
    Label confirmNewPasswordErrorLabel;
    @FXML
    Label newPasswordErrorLabel;
    @FXML
    Label oldPasswordErrorLabel;
    @FXML
    Label usernameErrorLabel;


    private void submitBreakAction() {
        if (addNewVacationCheckBox.isSelected())
            WeekManager.getInstance().addBreak(breakBeginDatePicker.getValue(), breakEndDatePicker.getValue(),
                    descriptionTextField.getText());
        else
            WeekManager.getInstance().updateBreak(currentBreak, breakBeginDatePicker.getValue(),
                    breakEndDatePicker.getValue(),
                    descriptionTextField.getText());
        addNewVacationCheckBox.setSelected(false);
        addNewVacationCheckBoxAction();
    }

    private void addNewVacationCheckBoxAction() {
        if(addNewVacationCheckBox.isSelected()) {
            File file = new File("src/resources/images/add.png");
            Image image = new Image(file.toURI().toString());
            addUpdateImageView.setImage(image);
            file = new File("src/resources/images/clear.png");
            image = new Image(file.toURI().toString());
            deleteImageView.setImage(image);
            breakBeginDatePicker.setValue(null);
            breakEndDatePicker.setValue(null);
            descriptionTextField.clear();
            leftArrowImageView.setVisible(false);
            rightArrowImageView.setVisible(false);
            breakSelectionLabel.setVisible(false);
        }
        else {
            File file = new File("src/resources/images/update.png");
            Image image = new Image(file.toURI().toString());
            addUpdateImageView.setImage(image);
            file = new File("src/resources/images/delete.png");
            image = new Image(file.toURI().toString());
            deleteImageView.setImage(image);
            leftArrowImageView.setVisible(true);
            rightArrowImageView.setVisible(true);
            breakSelectionLabel.setVisible(true);
            initializeView(false);
        }
    }

    private Boolean validatePasswordChange() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        if (oldPasswordField.getText().length() == 0) {
            oldPasswordErrorLabel.setText("Invalid old password!");
            oldPasswordField.setStyle(errorStyle);
            errors = true;
        }
        if (newPasswordField.getText().length() == 0) {
            newPasswordErrorLabel.setText("Invalid new password!");
            newPasswordField.setStyle(errorStyle);
            errors = true;
        }
        if (confirmNewPasswordField.getText().length() == 0) {
            confirmNewPasswordErrorLabel.setText("Invalid confirm password!");
            confirmNewPasswordField.setStyle(errorStyle);
            errors = true;
        }
        if (errors) {
            return false;
        } else {
            if (!oldPasswordField.getText().equals(user.getPassword())) {
                oldPasswordErrorLabel.setText("Incorrect old password!");
                oldPasswordField.setStyle(errorStyle);
                errors = true;
            }
            if (user.getPassword().equals(newPasswordField.getText())) {
                newPasswordErrorLabel.setText("New password must not be the old one!");
                newPasswordField.setStyle(errorStyle);
                errors = true;
            }
            if (!newPasswordField.getText().equals(confirmNewPasswordField.getText())) {
                confirmNewPasswordErrorLabel.setText("Passwords don't match!");
                confirmNewPasswordField.setStyle(errorStyle);
                errors = true;
            }
            return !errors;
        }
    }
    private void submitUserChangesAction() throws IOException {
        if(validatePasswordChange()) {
            userService.updateUser(usernameTextField.getText(), newPasswordField.getText());
            user.setPassword(newPasswordField.getText());
            clearFormAction();
            AlertViewController.showAlert(Alert.AlertType.INFORMATION, "Password reset was successful!");
        }
    }

    private void userDetailsRequestedAction() throws IOException {
        User user = (User) usersTableView.getSelectionModel().getSelectedItem();
        if(user == null)
            return;
        if(user.equals(this.user))
            throw new ValidationException("Cannot edit current user from here!");
        initializeUserDetailsView(ChangeEventType.UPDATE, user);
    }

    private void clearFormAction() {
        oldPasswordField.clear();
        newPasswordField.clear();
        confirmNewPasswordField.clear();
    }

    private void rightArrowAction() {
        currentBreak++;
        initializeView(false);
    }

    private void leftArrowAction() {
        currentBreak--;
        initializeView(false);
    }

    public void handleSubmitChangesImageViewOnAction(MouseEvent actionEvent) throws IOException {
        submitUserChangesAction();
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private void initializeView(Boolean modelChanged) {
        if(modelChanged)
            initializeModel();


        while (currentBreak < 1)
            currentBreak++;
        while (currentBreak > breakList.size())
            currentBreak--;
        if(breakList.size() > 0) {
            breakSelectionLabel.setText(currentBreak + "/" +
                    String.valueOf(WeekManager.getInstance().getAllBreaks().spliterator().getExactSizeIfKnown()));
            breakBeginDatePicker.setValue(breakList.get(currentBreak - 1).getStart());
            breakEndDatePicker.setValue(breakList.get(currentBreak - 1).getFinish());
            descriptionTextField.setText(breakList.get(currentBreak - 1).getDescription());
        }
        else {
            breakSelectionLabel.setText("0/0");
            addNewVacationCheckBox.setSelected(true);
            breakBeginDatePicker.setValue(null);
            breakEndDatePicker.setValue(null);
            addNewVacationCheckBoxAction();
        }
        usersTableView.setItems(users);
    }

    private void initializeModel() {
        breakList = StreamSupport.stream(WeekManager.getInstance().getAllBreaks().spliterator(), false)
                .collect(Collectors.toList());
        users.setAll(StreamSupport.stream(
                userService.getAllUsers().spliterator(), false)
                .collect(Collectors.toList()));
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserDetailsViewController(UserDetailsViewController userDetailsViewController) {
        this.userDetailsViewController = userDetailsViewController;
    }

    public void handleTableViewOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        if(mouseEvent.getClickCount() == 2) {
            try {
                userDetailsRequestedAction();
            }
            catch (InexistentUserException | ValidationException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
            }
        }
    }


    private void initializeUserDetailsView(ChangeEventType eventType, User userToSet) throws IOException {
        Stage userDetailsWindow = new Stage();
        userDetailsWindow.initStyle(StageStyle.UNDECORATED);
        userDetailsWindow.initModality(Modality.APPLICATION_MODAL);
        FXMLLoader userDetailsViewLoader = new FXMLLoader();
        userDetailsViewLoader.setLocation(getClass().getResource("../resources/views/userDetailsView.fxml"));
        BorderPane rootUserDetails = userDetailsViewLoader.load();
        rootUserDetails.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        rootUserDetails.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                userDetailsWindow.setX(event.getScreenX() - xOffset);
                userDetailsWindow.setY(event.getScreenY() - yOffset);
            }
        });
        userDetailsViewController = userDetailsViewLoader.getController();
        userDetailsViewController.setAdmin(user);
        userDetailsViewController.setUser(userToSet);
        userDetailsViewController.setUserService(userService);
        userDetailsWindow.setScene(new Scene(rootUserDetails));
        userDetailsViewController.setStage(userDetailsWindow);
        userDetailsViewController.setEventType(eventType);
        userDetailsViewController.init();
        userDetailsWindow.show();
    }


    public void handleAddNewUserMenuItemOnAction(ActionEvent actionEvent) throws IOException {
        initializeUserDetailsView(ChangeEventType.ADD, null);
    }

    public void handleClearFormImageViewOnAction(MouseEvent actionEvent) {
        clearFormAction();
    }


    @Override
    public void update(EntityChangeEvent<HasID> changeEvent) {
        initializeView(true);
    }

    public void handleSubmitImageViewOnAction(MouseEvent actionEvent) throws IOException {
        try {
            submitBreakAction();
        }
        catch (ValidationException | OverlappingBreaksException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }

    }

    public void handleRightArrowOnMouseClicked(MouseEvent mouseEvent) {
        rightArrowAction();
    }

    public void handleAddNewVacationCheckBoxOnAction(ActionEvent actionEvent) {
        addNewVacationCheckBoxAction();
    }

    public void handleLeftArrowOnMouseClicked(MouseEvent mouseEvent) {
        leftArrowAction();
    }


    public void handleAcademicYearDatePickerOnAction(ActionEvent actionEvent) throws IOException {
        WeekManager.getInstance().setUniversityYearBegining(academicYearDatePicker.getValue());
        ButtonType res = AlertViewController.showAlert(Alert.AlertType.CONFIRMATION, "Vacation may be invalid after this change. " +
                "Do you want to delete current existing vacations?");

        if(res == ButtonType.OK)
            WeekManager.getInstance().deleteAllBreaks();
    }

    public void handleClearDeleteBreakImageViewOnAction(MouseEvent actionEvent) {
        if(!addNewVacationCheckBox.isSelected()) {
            WeekManager.getInstance().deleteBreak(currentBreak);
            initializeView(true);
        }
        else {
            breakBeginDatePicker.setValue(null);
            breakEndDatePicker.setValue(null);
            descriptionTextField.setText(null);
        }
    }

    public void init() {
        academicYearDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.getDayOfWeek() != DayOfWeek.MONDAY);
            }
        });
        breakBeginDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.getDayOfWeek() != DayOfWeek.MONDAY);
            }
        });
        breakEndDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                setDisable(empty || date.getDayOfWeek() != DayOfWeek.SUNDAY);
            }
        });
        academicYearDatePicker.setValue(WeekManager.getInstance().getUniversityYearBegining());

        usernameTextField.setText(user.getID());

        switch (user.getUserRole()) {
            case ADMIN:
                timeSettingsTab.setDisable(true);
                accountsTab.setDisable(false);
                break;
            case TEACHER:
                timeSettingsTab.setDisable(true);
                accountsTab.setDisable(true);
                break;
            case SECRETARY:
                timeSettingsTab.setDisable(false);
                accountsTab.setDisable(true);
                break;
            default:
                break;
        }

        usernameTableViewColumn.setCellValueFactory(new PropertyValueFactory<User, String>("ID"));
        roleTableViewColumn.setCellValueFactory(new PropertyValueFactory<User, UserRole>("userRole"));
        nameTableViewColumn.setCellValueFactory(new PropertyValueFactory<User, String>("name"));



        initializeView(true);
    }


    public void handleTextFieldKeyTyped(KeyEvent keyEvent) {
        TextField field = ((TextField)keyEvent.getSource());
        field.setStyle(null);
        if(field.equals(oldPasswordField))
            oldPasswordErrorLabel.setText(null);
        else if(field.equals(newPasswordField))
            newPasswordErrorLabel.setText(null);
        else
            confirmNewPasswordErrorLabel.setText(null);
    }

    public void handleDeleteUserMenuItemOnAction(ActionEvent actionEvent) throws IOException {
        try {
            deleteUserAction();
        }
        catch (ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }

    private void deleteUserAction() {
        User user = (User)usersTableView.getSelectionModel().getSelectedItem();
        if(user == null)
            return;
        if(user.equals(this.user))
            throw new ValidationException("Cannot delete current user from here!");
        userService.deleteUser(user.getID());
    }
}
