package gui;

import domain.User;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.UserService;
import utilities.ChangeEventType;
import utilities.UserRole;
import validation.ExistentUserException;
import validation.ValidationException;

import java.io.IOException;


public class UserDetailsViewController {
    private Stage userDetailsWindow;
    private User user, admin;
    private ChangeEventType eventType;
    private UserService userService;
    private ObservableList<UserRole> userRoles = FXCollections.observableArrayList(UserRole.values());

    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField userPasswordField;
    @FXML
    TextField nameTextField;
    @FXML
    ComboBox roleComboBox;
    @FXML
    PasswordField adminPasswordField;
    @FXML
    Label usernameErrorLabel;
    @FXML
    Label userPasswordErrorLabel;
    @FXML
    Label nameErrorLabel;
    @FXML
    Label adminPasswordErrorLabel;

    private void initializeView() {

        roleComboBox.setItems(userRoles);
        if(eventType == ChangeEventType.ADD)
            usernameTextField.setEditable(true);
        else {
            usernameTextField.setEditable(false);
            usernameTextField.setText(user.getID());
            userPasswordField.setText(user.getPassword());
            nameTextField.setText(user.getName());
            roleComboBox.getSelectionModel().select(user.getUserRole());
        }
    }

    private void submitAction() {
        switch (eventType) {
            case ADD:
                userService.addUser(usernameTextField.getText(),
                        userPasswordField.getText(),
                        (UserRole) roleComboBox.getSelectionModel().getSelectedItem(),
                        nameTextField.getText());
                userDetailsWindow.close();
                break;
            case UPDATE:
                userService.updateUser(usernameTextField.getText(),
                        userPasswordField.getText(),
                        (UserRole) roleComboBox.getSelectionModel().getSelectedItem(),
                        nameTextField.getText());
                userDetailsWindow.close();
                break;
            default:
                break;
        }
    }

    public void handleEnterKeyPressed(KeyEvent keyEvent) throws IOException {
        ((BorderPane)keyEvent.getSource()).requestFocus();
        if(!validateSubmit())
            return;
        try {
            submitAction();
        }
        catch (ExistentUserException | ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }

    private boolean validateSubmit() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        if (usernameTextField.getText().length() == 0) {
            usernameErrorLabel.setText("Invalid username!");
            usernameTextField.setStyle(errorStyle);
            errors = true;
        }
        if (userPasswordField.getText().length() == 0) {
            userPasswordErrorLabel.setText("Invalid user password!");
            userPasswordField.setStyle(errorStyle);
            errors = true;
        }
        if (nameTextField.getText().length() == 0) {
            nameErrorLabel.setText("Invalid name!");
            nameTextField.setStyle(errorStyle);
            errors = true;
        }
        if (adminPasswordField.getText().length() == 0) {
            adminPasswordErrorLabel.setText("Invalid admin password!");
            adminPasswordField.setStyle(errorStyle);
            errors = true;
        }
        else if(!admin.getPassword().equals(adminPasswordField.getText())) {
            adminPasswordErrorLabel.setText("Incorrect admin password!");
            adminPasswordField.setStyle(errorStyle);
            errors = true;
        }
        return !errors;
    }


    public void handleMinimizeMouseClicked(MouseEvent mouseEvent) {
        userDetailsWindow.setIconified(true);
    }

    public void handleExitMouseClicked(MouseEvent mouseEvent) {
        userDetailsWindow.close();
    }

    public void handleTextFieldKeyTyped(KeyEvent keyEvent) {
        TextField field = ((TextField)keyEvent.getSource());
        field.setStyle(null);
        if(field.equals(nameTextField))
            nameErrorLabel.setText(null);
        else if(field.equals(usernameTextField))
            usernameErrorLabel.setText(null);
        else if(field.equals(userPasswordField))
            userPasswordErrorLabel.setText(null);
        else if(field.equals(adminPasswordField))
            adminPasswordErrorLabel.setText(null);
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setStage(Stage userDetailsWindow) {
        this.userDetailsWindow = userDetailsWindow;
    }

    public void setEventType(ChangeEventType eventType) {
        this.eventType = eventType;
    }

    public void setAdmin(User admin) {
        //System.out.println("aaaaa");
        this.admin = admin;
    }

    public void init() {
        initializeView();
    }
}
