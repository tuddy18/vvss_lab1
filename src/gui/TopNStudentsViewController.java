package gui;

import domain.Student;
import domain.StudentAverageDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import service.GradeService;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


public class TopNStudentsViewController {
    private GradeService gradeService;
    private ObservableList<StudentAverageDTO> studentAverageDTOS = FXCollections.observableArrayList();
    @FXML
    TextField numberOfStudentsTextField;
    @FXML
    TableView topNStudentsTableView;
    @FXML
    TableColumn studentIDTableViewColumn;
    @FXML
    TableColumn studentNameTableViewColumn;
    @FXML
    TableColumn groupTableViewColumn;
    @FXML
    TableColumn averageGradeTableViewColumn;

    private void initializeView(Boolean modelChanged, Integer nr) {
        if(modelChanged)
            initializeModel(nr);

        studentIDTableViewColumn.setCellValueFactory(new PropertyValueFactory<StudentAverageDTO, Integer>("id"));
        studentNameTableViewColumn.setCellValueFactory(new PropertyValueFactory<StudentAverageDTO, String>("name"));
        groupTableViewColumn.setCellValueFactory(new PropertyValueFactory<StudentAverageDTO, Short>("group"));
        averageGradeTableViewColumn.setCellValueFactory(new PropertyValueFactory<StudentAverageDTO, Double>("average"));

        topNStudentsTableView.setItems(studentAverageDTOS);
    }

    private void initializeModel(Integer nr) {
        Map<Student, Double> topN = gradeService.getTopNStudents(nr);
        studentAverageDTOS.setAll(StreamSupport.stream(topN.entrySet().spliterator(), false)
                .map(x -> new StudentAverageDTO(x.getKey().getID(), x.getKey().getName(), x.getKey().getGroup(), x.getValue()))
                .collect(Collectors.toList()));
    }

    public void setGradeService(GradeService gradeService) {
        this.gradeService = gradeService;
    }

    public void handleNumberOfStudentsTextFieldOnKeyPressed(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            try {
                initializeView(true, Integer.valueOf(numberOfStudentsTextField.getText()));
            }
            catch (NumberFormatException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, "Invalid number!");
            }
        }
    }

    public void handleAnonymousCheckBoxOnAction(ActionEvent actionEvent) {
        CheckBox checkBox = (CheckBox)actionEvent.getSource();
        if(checkBox.isSelected()) {
            studentNameTableViewColumn.setVisible(false);
        }
        else {
            studentNameTableViewColumn.setVisible(true);
        }
    }

    public void init() {
        initializeView(false, null);
    }
}
