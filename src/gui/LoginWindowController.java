package gui;

import domain.User;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.UserService;
import utilities.UserRole;

import javax.security.auth.login.LoginException;
import java.io.IOException;

public class LoginWindowController {
    private Stage primaryStage;
    private UserService userService;
    private MainWindowController mainWindowController;
    private double xOffset = 0;
    private double yOffset = 0;

    @FXML
    TextField usernameTextField;
    @FXML
    PasswordField loginPasswordField;

    private void initializeMainWindow(User user) throws IOException {
        FXMLLoader mainWindowLoader = new FXMLLoader();
        mainWindowLoader.setLocation(getClass().getResource(
                "/resources/views/mainWindow.fxml"));
        BorderPane rootLayout = mainWindowLoader.load();
        mainWindowController = mainWindowLoader.getController();
        Scene mainMenuScene = new Scene(rootLayout);
        primaryStage.setScene(mainMenuScene);
        mainWindowController.setPrimaryStage(primaryStage);
        mainWindowController.setUserService(userService);
        mainWindowController.setUser(user);
        mainWindowController.init();
        rootLayout.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        rootLayout.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                primaryStage.setX(event.getScreenX() - xOffset);
                primaryStage.setY(event.getScreenY() - yOffset);
            }
        });
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void handleKeyPressed(KeyEvent keyEvent) throws IOException {
        //initializeMainWindow(new User("su", "admin", UserRole.UNDEFINED, "SU"));
        if(keyEvent.getCode() == KeyCode.ENTER)
            try {
                User user = null;
                if(!bypassLogin()){
                    user = userService.performLogin(usernameTextField.getText(), loginPasswordField.getText());
                }
                else{
                    switch(usernameTextField.getText()){
                        case "profu":
                            user = new User("teacher", "1234", UserRole.TEACHER, "profu");
                            break;
                        case "adminu":
                            user = new User("admin", "1234", UserRole.ADMIN, "adminu");
                            break;
                        case "superuseru":
                            user = new User("superuser", "1234", UserRole.SUPERUSER, "superuseru");
                            break;
                        case "secretara":
                            user = new User("secretara", "1234", UserRole.SECRETARY, "secretara");
                            break;
                    }

                    //userService.addUser("teacher", "1234", UserRole.TEACHER, "profu");
                    //user = new User("Pop Florin", "1234", UserRole.TEACHER, "Pop Florin");
                }
                initializeMainWindow(user);
            } catch (LoginException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, "Invalid login credentials! " +
                        "Reenter credentials or contact the admin!");
            }
    }

    public boolean bypassLogin(){
        return true;
    }

    public void handleExitOnMouseClicked(MouseEvent mouseEvent) {
        primaryStage.close();
    }
}
