package gui;

import domain.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TabPane;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import repository.CrudRepository;
import repository.db.*;
import repository.xml.HomeworkXMLRepository;
import repository.xml.MotivatedAbsenceXMLRepository;
import repository.xml.StudentXMLRepository;
import service.*;
import validation.*;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MainWindowController {
    private Stage primaryStage;
    private TabPane gradesView;
    private TabPane studentsView;
    private AnchorPane homeworkView;
    private AnchorPane homeView;
    private BorderPane reportsView;
    private TabPane settingsView;
    private StudentService studentService;
    private HomeworkService homeworkService;
    private UserService userService;
    private MotivatedAbsenceService motivatedAbsenceService;
    private GradeService gradeService;
    private UserFeedbackTemplateService userFeedbackTemplateService;
    private StudentsViewController studentsViewController;
    private GradesViewController gradesViewController;
    private HomeworkViewController homeworkViewController;
    private HomeViewController homeViewController;
    private ReportsViewController reportsViewController;
    private SettingsViewController settingsViewController;
    private User user;
    private double unavailable = 0.5, available = 1.0;
    @FXML
    ImageView homeImageView;
    @FXML
    ImageView studentsImageView;
    @FXML
    ImageView homeworkImageView;
    @FXML
    ImageView gradesImageView;
    @FXML
    ImageView reportsImageView;
    @FXML
    ImageView settingsImageView;

    private void initializeSettingsView() throws IOException {
        FXMLLoader settingsViewLoader = new FXMLLoader();
        settingsViewLoader.setLocation(getClass().getResource(
                "/resources/views/settingsView.fxml"));
        TabPane rootSettings = settingsViewLoader.load();
        settingsViewController = settingsViewLoader.getController();
        settingsViewController.setUser(user);
        settingsViewController.setUserService(userService);
        settingsViewController.init();
        setSettingsView(rootSettings);
    }

    private void initializeHomeView() throws IOException {
        FXMLLoader homeViewLoader = new FXMLLoader();
        homeViewLoader.setLocation(getClass().getResource(
                "/resources/views/homeView.fxml"));
        AnchorPane rootHome = homeViewLoader.load();
        homeViewController = homeViewLoader.getController();
        homeViewController.setUser(user);
        homeViewController.setPrimaryStage(primaryStage);
        homeViewController.setUserService(userService);
        homeViewController.init();
        setHomeView(rootHome);
    }

    private void initializeHomeworkView() throws IOException {
        FXMLLoader homeworkViewLoader = new FXMLLoader();
        homeworkViewLoader.setLocation(getClass().getResource(
                "/resources/views/homeworkView.fxml"));
        AnchorPane rootHomework = homeworkViewLoader.load();
        homeworkViewController = homeworkViewLoader.getController();
        homeworkViewController.setHomeworkService(homeworkService);
        homeworkViewController.init();
        setHomeworkView(rootHomework);
    }

    private void initializeGradesView() throws IOException {
        FXMLLoader gradesViewLoader = new FXMLLoader();
        gradesViewLoader.setLocation(getClass().getResource(
                "/resources/views/gradesView.fxml"));
        TabPane rootGrades = gradesViewLoader.load();
        gradesViewController = gradesViewLoader.getController();
        gradesViewController.setStudentService(studentService);
        gradesViewController.setHomeworkService(homeworkService);
        gradesViewController.setGradeService(gradeService);
        gradesViewController.setUserFeedbackTemplateService(userFeedbackTemplateService);
        gradesViewController.setUser(user);
        gradesViewController.init();
        setGradesView(rootGrades);
    }

    private void initializeReportsView() throws IOException {
        FXMLLoader reportsViewLoader = new FXMLLoader();
        reportsViewLoader.setLocation(getClass().getResource(
                "/resources/views/reportsView.fxml"));
        BorderPane rootReports = reportsViewLoader.load();
        reportsViewController = reportsViewLoader.getController();
        reportsViewController.setReportsView(rootReports);
        reportsViewController.setHomeworkService(homeworkService);
        reportsViewController.setStudentService(studentService);
        reportsViewController.setGradeService(gradeService);
        reportsViewController.init();
        setReportsView(rootReports);
    }

    private void initializeStudentsView() throws IOException {
        FXMLLoader studentsViewLoader = new FXMLLoader();
        studentsViewLoader.setLocation(getClass().getResource(
                "/resources/views/studentsView.fxml"));
        TabPane rootStudents = studentsViewLoader.load();
        studentsViewController = studentsViewLoader.getController();
        studentsViewController.setMotivatedAbsenceService(motivatedAbsenceService);
        studentsViewController.setStudentService(studentService);
        studentsViewController.init();
        setStudentsView(rootStudents);
    }

    private void initializeView() throws IOException {
        initializeHomeView();
        initializeSettingsView();
        switch (user.getUserRole()) {
            case ADMIN:
                break;
            case TEACHER:
                initializeHomeworkView();
                initializeGradesView();
                initializeReportsView();
                break;
            case SECRETARY:
                initializeStudentsView();
                initializeGradesView();
                initializeReportsView();
                break;
            default:
                initializeStudentsView();
                initializeHomeworkView();
                initializeGradesView();
                initializeReportsView();
                break;
        }

        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(homeView);

        WeekManager.getInstance().addObserver(settingsViewController);
        switch (user.getUserRole()) {
            case ADMIN:
                userService.addObserver(settingsViewController);
                break;
            case TEACHER:
                studentService.addObserver(gradesViewController);
                gradeService.addObserver(gradesViewController);
                homeworkService.addObserver(gradesViewController);
                homeworkService.addObserver(homeworkViewController);
                homeworkService.addObserver(reportsViewController);
                userFeedbackTemplateService.addObserver(gradesViewController);
                break;
            case SECRETARY:
                studentService.addObserver(studentsViewController);
                studentService.addObserver(gradesViewController);
                gradeService.addObserver(gradesViewController);
                homeworkService.addObserver(gradesViewController);
                homeworkService.addObserver(reportsViewController);
                break;
            default:
                studentService.addObserver(studentsViewController);
                studentService.addObserver(gradesViewController);
                gradeService.addObserver(gradesViewController);
                homeworkService.addObserver(gradesViewController);
                homeworkService.addObserver(homeworkViewController);
                homeworkService.addObserver(reportsViewController);
                userService.addObserver(settingsViewController);
                userFeedbackTemplateService.addObserver(gradesViewController);
                break;
        }
    }

    private void initializeBreakData() throws IOException {
        Validator<Break> breakValidator;
        CrudRepository<Integer, Break> breakCrudRepository;
        breakValidator = new BreakValidator();
        breakCrudRepository = new BreakDatabaseRepository("Breaks", breakValidator, true);
        WeekManager.getInstance().setBreakCrudRepository(breakCrudRepository);
    }

    private void initializeStudentData() throws IOException {
        Validator<Student> studentValidator;
        CrudRepository<Integer, Student> studentRepo;
        CrudRepository<Integer, Student> deletedStudentRepo;
        studentValidator = new StudentValidator();
        studentRepo = new StudentDatabaseRepository("Students", studentValidator, true);
        deletedStudentRepo = new StudentDatabaseRepository("FormerStudents", studentValidator, true);
        studentService = new StudentService(studentRepo, deletedStudentRepo);
    }

    private void initializeHomeworkData() throws IOException {
        Validator<Homework> homeworkValidator;
        CrudRepository<Integer, Homework> homeworkRepo;
        homeworkValidator = new HomeworkValidator();
        homeworkRepo = new HomeworkDatabaseRepository("Homework", homeworkValidator, true);
        homeworkService = new HomeworkService(homeworkRepo);
    }

    private void initializeGradeFeedbackTemplateData() throws IOException {
        Validator<UserFeedbackTemplate> userFeedbackTemplateValidator;
        CrudRepository<String, UserFeedbackTemplate> userFeedbackTemplateRepo;
        userFeedbackTemplateValidator = new UserFeedbackValidator();
        userFeedbackTemplateRepo = new UserFeedbackTemplateDatabaseRepository("userfeedbacktemplate",
                userFeedbackTemplateValidator, true);
        userFeedbackTemplateService = new UserFeedbackTemplateService(userFeedbackTemplateRepo);
    }

    private void initializeMotivatedAbsenceData() throws IOException {
        Validator<Student> studentValidator;
        Validator<MotivatedAbsence> motivatedAbsenceValidator;
        CrudRepository<Integer, Student> studentRepo;
        CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository;
        studentValidator = new StudentValidator();
        motivatedAbsenceValidator = new MotivatedAbsenceValidator();
        studentRepo = new StudentDatabaseRepository("Students", studentValidator, true);
        motivatedAbsenceCrudRepository = new MotivatedAbsenceDatabaseRepository("MotivatedAbsences",
                motivatedAbsenceValidator, studentRepo, true);
        motivatedAbsenceService = new MotivatedAbsenceService(motivatedAbsenceCrudRepository, studentRepo);
    }

    private void initializeGradeData() throws IOException {
        Validator<Student> studentValidator;
        Validator<Homework> homeworkValidator;
        Validator<MotivatedAbsence> motivatedAbsenceValidator;
        CrudRepository<Integer, Student> studentRepo;
        CrudRepository<Integer, Homework> homeworkRepo;
        CrudRepository<Integer, MotivatedAbsence> motivatedAbsenceCrudRepository;
        studentValidator = new StudentValidator();
        homeworkValidator = new HomeworkValidator();
        motivatedAbsenceValidator = new MotivatedAbsenceValidator();
        studentRepo = new StudentDatabaseRepository("Students", studentValidator, true);
        homeworkRepo = new HomeworkDatabaseRepository("Homework", homeworkValidator, true);
        motivatedAbsenceCrudRepository = new MotivatedAbsenceDatabaseRepository("MotivatedAbsences",
                motivatedAbsenceValidator, studentRepo, true);
        gradeService = new GradeService(studentRepo, homeworkRepo, motivatedAbsenceCrudRepository);
    }

    private void initializeData() throws IOException {
        initializeBreakData();
        switch (user.getUserRole()) {
            case ADMIN:
                break;
            case TEACHER:
                initializeHomeworkData();
                initializeGradeData();
                initializeStudentData();
                initializeGradeFeedbackTemplateData();
                break;
            case SECRETARY:
                initializeStudentData();
                initializeHomeworkData();
                initializeGradeData();
                initializeMotivatedAbsenceData();
                break;
            default:
                initializeStudentData();
                initializeHomeworkData();
                initializeGradeData();
                initializeMotivatedAbsenceData();
                initializeGradeFeedbackTemplateData();
                break;
        }
    }

    public void setGradesView(TabPane gradesView) {
        this.gradesView = gradesView;
    }

    public void setHomeworkView(AnchorPane homeworkView) {
        this.homeworkView = homeworkView;
    }

    public void setStudentsView(TabPane studentsView) {
        this.studentsView = studentsView;
    }

    public void setHomeView(AnchorPane homeView) {
        this.homeView = homeView;
    }

    public void setSettingsView(TabPane settingsView) {
        this.settingsView = settingsView;
    }

    public void setReportsView(BorderPane reportsView) {
        this.reportsView = reportsView;
    }


    public MainWindowController() throws IOException {

    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setUser(User user) throws IOException {
        this.user = user;

        switch (user.getUserRole()) {
            case ADMIN:
                studentsImageView.setOpacity(unavailable);
                homeworkImageView.setOpacity(unavailable);
                gradesImageView.setOpacity(unavailable);
                reportsImageView.setOpacity(unavailable);
                settingsImageView.setOpacity(available);
                break;
            case TEACHER:
                studentsImageView.setOpacity(unavailable);
                homeworkImageView.setOpacity(available);
                gradesImageView.setOpacity(available);
                reportsImageView.setOpacity(available);
                settingsImageView.setOpacity(available);
                break;
            case SECRETARY:
                studentsImageView.setOpacity(available);
                homeworkImageView.setOpacity(unavailable);
                gradesImageView.setOpacity(available);
                reportsImageView.setOpacity(available);
                settingsImageView.setOpacity(available);
                break;
            default:
                break;
        }
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void handleGradesMouseClicked(MouseEvent mouseEvent) {
        if(gradesImageView.getOpacity() == unavailable)
            return;
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(gradesView);
    }

    public void handleMinimizedMouseClicked(MouseEvent mouseEvent) {
        primaryStage.setIconified(true);
    }

    public void handleExitMouseClicked(MouseEvent mouseEvent) {
        primaryStage.close();
    }


    public void handleStudentsMouseClicked(MouseEvent mouseEvent) {
        if(studentsImageView.getOpacity() == unavailable)
            return;
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(studentsView);
    }

    public void handleHomeworkMouseClicked(MouseEvent mouseEvent) {
        if(homeworkImageView.getOpacity() == unavailable)
            return;
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(homeworkView);
    }

    public void handleReportsMouseClicked(MouseEvent mouseEvent) {
        if(reportsImageView.getOpacity() == unavailable)
            return;
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(reportsView);
    }

    public void handleSettingsMouseClicked(MouseEvent mouseEvent) {
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(settingsView);
    }

    public void handleHomeMouseClicked(MouseEvent mouseEvent) {
        ((BorderPane) primaryStage.getScene().getRoot()).setCenter(homeView);
    }

    public void init() throws IOException {
        initializeData();
        initializeView();
    }
}
