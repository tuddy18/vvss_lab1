package gui;

import domain.User;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import service.UserService;

import java.io.IOException;

public class HomeViewController {
    private User user;
    private Stage primaryStage;
    private LoginWindowController loginWindowController;
    private UserService userService;

    @FXML
    Label welcomeLabel;
    @FXML
    Label roleLabel;

    private void initializeView() {
        welcomeLabel.setText("Welcome to the platform, " + user.getName() + "!");
        roleLabel.setText("Logged in as: " + user.getUserRole());
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    public void init() {
        initializeView();
    }

    public void handleLogoutImageViewOnMouseClicked(MouseEvent mouseEvent) throws IOException {
        initializeLoginView();
    }

    private void initializeLoginView() throws IOException {
        FXMLLoader loginWindowLoader = new FXMLLoader();
        loginWindowLoader.setLocation(getClass().getResource(
                "/resources/views/loginWindow.fxml"));
        BorderPane rootLayout = loginWindowLoader.load();
        loginWindowController = loginWindowLoader.getController();
        Scene loginScene = new Scene(rootLayout);
        primaryStage.setScene(loginScene);
        loginWindowController.setPrimaryStage(primaryStage);
        loginWindowController.setUserService(userService);
        primaryStage.setScene(loginScene);
    }
}
