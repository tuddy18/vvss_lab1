package gui;

import domain.HasID;
import domain.MotivatedAbsence;
import domain.Student;
import domain.StudentDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import service.MotivatedAbsenceService;
import service.StudentService;
import service.WeekManager;
import utilities.ChangeEventType;
import utilities.EntityChangeEvent;
import utilities.Observer;
import validation.ExistentMotivatedAbsenceException;
import validation.InexistentMotivatedAbsenceException;
import validation.InexistentStudentException;
import validation.ValidationException;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


public class StudentsViewController implements Observer<EntityChangeEvent<HasID>>{
    private Stage studentViewStage;
    private StudentService studentService;
    private MotivatedAbsenceService motivatedAbsenceService;
    private StudentDetailsViewController studentDetailsViewController;
    private ObservableList<Student> model = FXCollections.observableArrayList();
    private boolean secondEnterPress;
    private double xOffset = 0;
    private double yOffset = 0;
    @FXML
    ListView studentsListView;
    @FXML
    RadioButton currentlyStudying;
    @FXML
    RadioButton formerStudying;
    @FXML
    RadioButton allStudying;
    @FXML
    ToggleGroup studyingStatus;
    @FXML
    TextField studentsTextField;
    @FXML
    MenuItem addStudentMenuItem;
    @FXML
    MenuItem deleteStudentMenuItem;
    @FXML
    MenuItem recoverStudentMenuItem;
    @FXML
    MenuItem permanentlyDeleteStudentMenuItem;

    @FXML
    ComboBox studentComboBox;
    @FXML
    DatePicker absenceDatePicker;
    @FXML
    TextArea reasonTextArea;
    @FXML
    CheckBox filterStudentsCheckBox;
    @FXML
    CheckBox viewExistentAbsencesCheckBox;
    @FXML
    ImageView addUpdateImageView;
    @FXML
    ImageView clearDeleteImageView;
    @FXML
    Label studentErrorLabel;
    @FXML
    Label absenceErrorLabel;
    @FXML
    Label reasonErrorLabel;

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void setMotivatedAbsenceService(MotivatedAbsenceService motivatedAbsenceService) {
        this.motivatedAbsenceService = motivatedAbsenceService;
    }

    @Override
    public void update(EntityChangeEvent studentEntityChangeEvent) {
        initializeView(true);
    }

    private void initializeModel() {
        Iterable<Student> studentIterable;
        RadioButton selected = (RadioButton) studyingStatus.getSelectedToggle();
        if(selected.equals(currentlyStudying))
            studentIterable = studentService.getAllStudents();
        else if(selected.equals(formerStudying))
            studentIterable = studentService.getAllDeletedStudents();
        else
            studentIterable = Stream.concat(
                    StreamSupport.stream(studentService.getAllStudents().spliterator(),false),
                    StreamSupport.stream(studentService.getAllDeletedStudents().spliterator(), false))
                    .collect(Collectors.toList());

        List<Student> studentList = StreamSupport.stream(studentIterable.spliterator(), false)
                .collect(Collectors.toList());
        model.setAll(studentList);
    }

    private void initializeView(Boolean modelChanged) {
        if(modelChanged)
            initializeModel();
        String studentName = studentsTextField.getText();
        ObservableList<StudentDTO> visibleStudentList = FXCollections.observableArrayList();
        visibleStudentList.setAll(model.stream()
                .filter(x->x.getName().contains(studentName))
                .map(x -> new StudentDTO(x.getID(), x.getName()))
                .collect(Collectors.toList()));
        studentsListView.setItems(visibleStudentList);
        absenceDatePicker.setPromptText("Select date");
        final String filterText;
        Object studentComboBoxValue = studentComboBox.getValue();
        if(studentComboBoxValue == null || studentComboBoxValue instanceof StudentDTO)
            filterText = "";
        else
            filterText = (String) studentComboBoxValue;
        System.out.println(filterText);
        ObservableList<StudentDTO> studentDTOS = FXCollections.observableArrayList();
        studentDTOS.setAll(model.stream()
                .filter(x -> x.getName().contains(filterText))
                .map(x -> new StudentDTO(x.getID(), x.getName()))
                .collect(Collectors.toList()));
        studentComboBox.setItems(studentDTOS);
    }

    private void clearDeleteAction() {
        if(viewExistentAbsencesCheckBox.isSelected()) {
            if(validateAbsence())
                try {
                    motivatedAbsenceService.deleteMotivatedAbsence(((StudentDTO)
                                studentComboBox.getSelectionModel().getSelectedItem()).getId(),
                        WeekManager.getInstance().dateToWeek(absenceDatePicker.getValue()));
                }
                catch (InexistentMotivatedAbsenceException e) {
                    e.printStackTrace();
                }
        }

        reasonTextArea.clear();
        absenceDatePicker.setValue(null);
        studentComboBox.getSelectionModel().clearSelection();
        studentComboBox.setPromptText("Select student");
        absenceDatePicker.setPromptText("Select week");

    }


    private void addUpdateAction() {
        if(!viewExistentAbsencesCheckBox.isSelected()) {
            //new stuff
            int id = Integer.parseInt(studentComboBox.getSelectionModel().getSelectedItem().toString().split("=")[1].
                    split("\\)")[0]);
            Student student = studentService.getStudent(id);
            StudentDTO studentDTO = new StudentDTO(student.getID(), student.getName());
            motivatedAbsenceService.addMotivatedAbsence(
                    id,
                    WeekManager.getInstance().dateToWeek(absenceDatePicker.getValue()),
                    reasonTextArea.getText());

            /*motivatedAbsenceService.addMotivatedAbsence(((StudentDTO)
                            studentComboBox.getSelectionModel().getSelectedItem()).getId(),
                    WeekManager.getInstance().dateToWeek(absenceDatePicker.getValue()),
                    reasonTextArea.getText());*/
            reasonTextArea.clear();
        }
        else
            motivatedAbsenceService.updateMotivatedAbsence(((StudentDTO)
                            studentComboBox.getSelectionModel().getSelectedItem()).getId(),
                    WeekManager.getInstance().dateToWeek(absenceDatePicker.getValue()),
                    reasonTextArea.getText());

        absenceDatePicker.setValue(null);
        studentComboBox.getSelectionModel().clearSelection();
        reasonTextArea.clear();
    }

    private boolean validateAbsence() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        if(studentComboBox.getSelectionModel().getSelectedItem() == null) {
            studentErrorLabel.setText("Invalid student selection!");
            studentComboBox.setStyle(errorStyle);
            errors = true;
        }
        if(absenceDatePicker.getValue() == null) {
            absenceErrorLabel.setText("Invalid absence selection!");
            absenceDatePicker.setStyle(errorStyle);
            errors = true;
        }
        if(reasonTextArea.getText().length() == 0) {
            reasonErrorLabel.setText("Invalid reason!");
            reasonTextArea.setStyle(errorStyle);
            errors = true;
        }
        return !errors;
    }

    public void handleStudyingRadioButtonClicked(MouseEvent mouseEvent) {
        RadioButton selected = (RadioButton) studyingStatus.getSelectedToggle();
        if(selected.equals(currentlyStudying)) {
            recoverStudentMenuItem.setDisable(true);
            permanentlyDeleteStudentMenuItem.setDisable(true);
            addStudentMenuItem.setDisable(false);
            deleteStudentMenuItem.setDisable(false);
        }
        else if(selected.equals(formerStudying)) {
            recoverStudentMenuItem.setDisable(false);
            permanentlyDeleteStudentMenuItem.setDisable(false);
            addStudentMenuItem.setDisable(true);
            deleteStudentMenuItem.setDisable(true);
        }
        else {
            recoverStudentMenuItem.setDisable(true);
            permanentlyDeleteStudentMenuItem.setDisable(true);
            addStudentMenuItem.setDisable(false);
            deleteStudentMenuItem.setDisable(true);
        }
        initializeView(true);
    }

    public void handleStudentTextFieldKeyTyped(KeyEvent keyEvent) {
        initializeView(false);
    }

    public void handleDeleteStudentMenuItemOnAction(ActionEvent actionEvent) {
        StudentDTO studentDTO = (StudentDTO) studentsListView.getSelectionModel().getSelectedItem();
        if(studentDTO == null)
            return;
        studentService.deleteStudent(studentDTO.getId());
    }

    private void initializeStudentDetailsViewWindow(ChangeEventType eventType, String windowTitle, Student student) throws IOException {
        Stage studentDetailsWindow = new Stage();
        studentDetailsWindow.initStyle(StageStyle.UNDECORATED);
        studentDetailsWindow.initModality(Modality.APPLICATION_MODAL);
        studentDetailsWindow.setTitle(windowTitle);
        FXMLLoader studentDetailsViewLoader = new FXMLLoader();
        studentDetailsViewLoader.setLocation(getClass().getResource("../resources/views/studentDetailsView.fxml"));
        BorderPane rootStudentDetails = studentDetailsViewLoader.load();
        rootStudentDetails.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        rootStudentDetails.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                studentDetailsWindow.setX(event.getScreenX() - xOffset);
                studentDetailsWindow.setY(event.getScreenY() - yOffset);
            }
        });
        studentDetailsViewController = studentDetailsViewLoader.getController();
        studentDetailsViewController.setEventType(eventType);
        studentDetailsViewController.setStudent(student);
        studentDetailsViewController.setStudentService(studentService);
        studentDetailsViewController.setStudentDetailsStage(studentDetailsWindow);
        studentDetailsWindow.setScene(new Scene(rootStudentDetails));
        studentDetailsViewController.init();
        studentDetailsWindow.show();
    }

    public void handleAddNewStudentMenuItemOnAction(ActionEvent actionEvent) throws IOException {
        initializeStudentDetailsViewWindow(ChangeEventType.ADD, "Add new student",null );
    }

    public void handleListViewElementMouseClicked(MouseEvent mouseEvent) throws IOException {
        if(mouseEvent.getClickCount() == 2) {
            try {
                StudentDTO studentDTO = (StudentDTO) studentsListView.getSelectionModel().getSelectedItem();
                if(studentDTO == null)
                    return;
                Student student = studentService.getStudent(studentDTO.getId());
                initializeStudentDetailsViewWindow(ChangeEventType.UPDATE, "Update student", student);
            }
            catch (InexistentStudentException e) {
            }
        }
    }

    public void handleRecoverStudentMenuItemOnAction(ActionEvent actionEvent) {
        StudentDTO studentDTO = (StudentDTO) studentsListView.getSelectionModel().getSelectedItem();
        if(studentDTO == null)
            return;
        studentService.recoverStudent(studentDTO.getId());
    }

    public void handlePermanentlyDeleteStudentMenuItemOnAction(ActionEvent actionEvent) {
        StudentDTO studentDTO = (StudentDTO) studentsListView.getSelectionModel().getSelectedItem();
        if(studentDTO == null)
            return;
        studentService.permanentlyDeleteStudent(studentDTO.getId());
    }

    public void setStudentViewStage(Stage studentViewStage) {
        this.studentViewStage = studentViewStage;
    }

    public void handleAddUpdateAbsenceImageViewMouseClicked(MouseEvent actionEvent) throws IOException {
        if(validateAbsence()) {
            try {
                addUpdateAction();
                AlertViewController.showAlert(Alert.AlertType.INFORMATION, "Motivated absence successfully submitted!");
            }
            catch (ValidationException | ExistentMotivatedAbsenceException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
            }

        }

    }


    public void handleSelectStudentComboBoxOnAction(ActionEvent actionEvent) {
        absenceDatePicker.setValue(null);
        studentComboBox.setStyle(null);
        studentErrorLabel.setText(null);
    }

    public void handleClearDeleteImageViewMouseClicked(MouseEvent actionEvent) throws IOException {
        try {
            clearDeleteAction();
            AlertViewController.showAlert(Alert.AlertType.INFORMATION, "Motivated absence successfully deleted!");
        }
        catch (InexistentMotivatedAbsenceException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }


    public void handleFilterEntriesCheckBoxOnAction(ActionEvent actionEvent) {
        if(filterStudentsCheckBox.isSelected()) {
            studentComboBox.setEditable(true);
            studentComboBox.setValue("");
            initializeView(false);
        }
        else {
            studentComboBox.setEditable(false);
        }
    }

    public void handleStudentsComboBoxKeyTyped(KeyEvent keyEvent) {
        if(keyEvent.getCode().equals(KeyCode.CONTROL)) {
            initializeView(false);
            studentComboBox.setEditable(false);
            filterStudentsCheckBox.setSelected(false);
        }
    }

    public void handleViewExistentAbsencesCheckBoxOnAction(ActionEvent actionEvent) {
        absenceDatePicker.setValue(null);
        if(viewExistentAbsencesCheckBox.isSelected()) {
            File file = new File("src/resources/images/update.png");
            Image image = new Image(file.toURI().toString());
            addUpdateImageView.setImage(image);
            file = new File("src/resources/images/delete.png");
            image = new Image(file.toURI().toString());
            clearDeleteImageView.setImage(image);
        }
        else {
            File file = new File("src/resources/images/add.png");
            Image image = new Image(file.toURI().toString());
            addUpdateImageView.setImage(image);
            file = new File("src/resources/images/clear.png");
            image = new Image(file.toURI().toString());
            clearDeleteImageView.setImage(image);
        }
    }


    public void handleAbsenceDatePickerOnAction(ActionEvent actionEvent) {
        absenceDatePicker.setStyle(null);
        absenceErrorLabel.setText(null);
        if(viewExistentAbsencesCheckBox.isSelected() &&
                studentComboBox.getSelectionModel().getSelectedItem() instanceof StudentDTO &&
                absenceDatePicker.getValue() != null)
            reasonTextArea.setText(motivatedAbsenceService.getAbsence(((StudentDTO)
                            studentComboBox.getSelectionModel().getSelectedItem()).getId(),
                    WeekManager.getInstance().dateToWeek(absenceDatePicker.getValue())).getReason());
    }

    public void init() {
        absenceDatePicker.setDayCellFactory(picker -> new DateCell() {
            @Override
            public void updateItem(LocalDate date, boolean empty) {
                super.updateItem(date, empty);
                Object selectedIt = studentComboBox.getSelectionModel().getSelectedItem();
                if(selectedIt == null || selectedIt instanceof String || date == null)
                    return;
                Byte week = WeekManager.getInstance().dateToWeek(date);
                Iterable<MotivatedAbsence> motivatedAbsences;
                motivatedAbsences = motivatedAbsenceService
                            .getAbsencesForStudent(((StudentDTO) selectedIt).getId());
                Boolean match = false;
                if(motivatedAbsences != null)
                    match = StreamSupport.stream(motivatedAbsences.spliterator(), false)
                        .anyMatch(a -> a.getWeek().equals(WeekManager.getInstance().dateToWeek(date)));
                setDisable(empty || !viewExistentAbsencesCheckBox.isSelected() ?
                        match || week == -1 ||  week > 14 : !match);
            }
        });
        initializeView(true);
        //selectWeekComboBox.setItems(weeks);
        handleStudyingRadioButtonClicked(null);
        //studentService.addObserver(this);
    }

    public void handleReasonTextAreaKeyTyped(KeyEvent keyEvent) {
        reasonTextArea.setStyle(null);
        reasonErrorLabel.setText(null);
    }
}
