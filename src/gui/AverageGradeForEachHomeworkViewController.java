package gui;

import domain.Homework;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import service.GradeService;
import service.HomeworkService;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class AverageGradeForEachHomeworkViewController {
    private GradeService gradeService;
    private HomeworkService homeworkService;
    @FXML
    BarChart averageGradeForEachHomeworkBarChart;

    private void initializeView() {
        averageGradeForEachHomeworkBarChart.setLegendVisible(false);
        XYChart.Series dataSeries = new XYChart.Series();
        averageGradeForEachHomeworkBarChart.getData().clear();
        Map<Homework, Double> homeworkAverages = gradeService.getAverageGradeForEachHomework();
        List<String> categories =  StreamSupport.stream(homeworkAverages.keySet().spliterator(), false)
                .map(Homework::getDescription)
                .collect(Collectors.toList());
        ((CategoryAxis)averageGradeForEachHomeworkBarChart.getXAxis())
                .setCategories(FXCollections.observableArrayList(categories));
        homeworkAverages.forEach((x, y) -> dataSeries.getData().add(new XYChart.Data(x.getDescription(), y)));
        averageGradeForEachHomeworkBarChart.getData().add(dataSeries);
    }

    public void setGradeService(GradeService gradeService) {
        this.gradeService = gradeService;
    }

    public void setHomeworkService(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    public void init() {
        initializeView();
    }
}
