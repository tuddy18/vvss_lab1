package gui;

import domain.HasID;
import domain.Homework;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import service.HomeworkService;
import utilities.EntityChangeEvent;
import utilities.Observer;
import validation.ExistentHomeworkException;
import validation.InvalidChangeException;
import validation.ValidationException;


import java.io.IOException;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class HomeworkViewController implements Observer<EntityChangeEvent<HasID>> {
    private HomeworkService homeworkService;
    private ObservableList<Homework> homework = FXCollections.observableArrayList();
    @FXML
    TableView homeworkTableView;
    @FXML
    TableColumn descriptionTableViewColumn;
    @FXML
    TableColumn assignationTableViewColumn;
    @FXML
    TableColumn deadlineTableViewColumn;
    @FXML
    TextField descriptionTextField;
    @FXML
    TextField assignationTextField;
    @FXML
    TextField deadlineTextField;
    @FXML
    Label descriptionErrorLabel;
    @FXML
    Label assignationErrorLabel;
    @FXML
    Label deadlineErrorLabel;


    private void initializeView(Boolean homeworkModelChanged) {
        if(homeworkModelChanged)
            initializeModel();

        homeworkTableView.setItems(homework);
    }

    private void initializeModel() {
        homework.setAll(StreamSupport.stream(
                homeworkService.getAllHomework().spliterator(), false)
                .collect(Collectors.toList()));
    }

    public void setHomeworkService(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }


    public void handlePostponeHomeworkOneWeekMenuItemOnAction(ActionEvent actionEvent) throws IOException {
        Homework homework = ((Homework)homeworkTableView.getSelectionModel().getSelectedItem());
        try {
            homeworkService.modifyHomeworkDeadline(homework.getID(), (byte)(homework.getDeadlineWeek() + 1));
        }
        catch (InvalidChangeException | ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }

    @Override
    public void update(EntityChangeEvent hasIDEntityChangeEvent) {
        System.out.println("aaa");
        initializeView(true);
    }

    public void handleAddImageViewOnAction(MouseEvent actionEvent) throws IOException {
        try {
            addHomeworkAction();
        }
        catch (ValidationException | ExistentHomeworkException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }

    public void handleTextFieldKeyTyped(KeyEvent keyEvent) {
        TextField field = ((TextField)keyEvent.getSource());
        field.setStyle(null);
        if(field.equals(descriptionTextField))
            descriptionErrorLabel.setText(null);
        else if(field.equals(assignationTextField))
            assignationErrorLabel.setText(null);
        else
            descriptionErrorLabel.setText(null);

    }

    private void addHomeworkAction() throws IOException {
        if(validateAddHomework()) {
            Byte assigned = Byte.valueOf(assignationTextField.getText());
            Byte deadline = Byte.valueOf(deadlineTextField.getText());
            try {
                homeworkService.addHomework(descriptionTextField.getText(), assigned, deadline);
            }
            catch (ExistentHomeworkException | ValidationException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
            }
            descriptionTextField.clear();
            assignationTextField.clear();
            deadlineTextField.clear();
        }
    }

    private boolean validateAddHomework() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        if (descriptionTextField.getText().length() == 0) {
            descriptionErrorLabel.setText("Invalid description!");
            descriptionTextField.setStyle(errorStyle);
            errors = true;
        }
        if (assignationTextField.getText().length() == 0) {
            assignationErrorLabel.setText("Invalid week");
            assignationTextField.setStyle(errorStyle);
            errors = true;
        }
        if (deadlineTextField.getText().length() == 0) {
            deadlineErrorLabel.setText("Invalid week!");
            deadlineTextField.setStyle(errorStyle);
            errors = true;
        }
        if(errors)
            return false;
        else {
            Byte assigned = 0, deadline = 0;
            try {
                assigned = Byte.valueOf(assignationTextField.getText());
            }
            catch (NumberFormatException e) {
                assignationErrorLabel.setText("Invalid assignation week!");
                assignationTextField.setStyle(errorStyle);
                errors = true;
            }
            try {
                deadline = Byte.valueOf(deadlineTextField.getText());
            }
            catch (NumberFormatException e) {
                deadlineErrorLabel.setText("Invalid assignation week!");
                deadlineTextField.setStyle(errorStyle);
                errors = true;
            }
            if(!errors && assigned >= deadline) {
                assignationErrorLabel.setText("Invalid period!");
                assignationTextField.setStyle(errorStyle);
                deadlineTextField.setStyle(errorStyle);
                errors = true;
            }
            return !errors;
        }
    }

    public void init() {
        descriptionTableViewColumn.setCellValueFactory(new PropertyValueFactory<Homework, String>("description"));
        assignationTableViewColumn.setCellValueFactory(new PropertyValueFactory<Homework, Byte>("assignedWeek"));
        deadlineTableViewColumn.setCellValueFactory(new PropertyValueFactory<Homework, Byte>("deadlineWeek"));

        initializeView(true);
    }

    public void handlePostponeByMoreWeeksTextFieldOnKeyPressed(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode() != KeyCode.ENTER)
            return;
        TextField t = (TextField)keyEvent.getSource();
        if(t.getText() == null || t.getText().isEmpty())
            return;
        Homework homework = ((Homework)homeworkTableView.getSelectionModel().getSelectedItem());
        Byte nrWeeks = Byte.valueOf(t.getText());
        try {
            homeworkService.modifyHomeworkDeadline(homework.getID(), (byte)(homework.getDeadlineWeek() + nrWeeks));
        }
        catch (InvalidChangeException | ValidationException e) {
            AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
        }
    }
}
