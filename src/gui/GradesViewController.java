package gui;

import domain.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import service.*;
import utilities.EntityChangeEvent;
import utilities.Observer;
import validation.CannotAddGradeException;
import validation.ExistentGradeException;
import validation.InexistentUserFeedbackTemplate;
import validation.ValidationException;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GradesViewController implements Observer<EntityChangeEvent<HasID>> {
    private StudentService studentService;
    private HomeworkService homeworkService;
    private GradeService gradeService;
    private UserFeedbackTemplateService userFeedbackTemplateService;
    private User user;
    private ObservableList<Student> students = FXCollections.observableArrayList();
    private ObservableList<Homework> homework = FXCollections.observableArrayList();
    private ObservableList<Grade> grades = FXCollections.observableArrayList();

    @FXML
    TabPane gradesTab;
    @FXML
    Tab addGradeTab;
    @FXML
    Tab viewGradesTab;

    //Add grade tab
    @FXML
    ComboBox studentComboBox;
    @FXML
    ComboBox homeworkComboBox;
    @FXML
    TextField gradeTextField;
    @FXML
    TextArea feedbackTextArea;
    @FXML
    CheckBox filterStudentsCheckBox;
    @FXML
    Label studentErrorLabel;
    @FXML
    Label homeworkErrorLabel;
    @FXML
    Label gradeErrorLabel;

    //View grades tab
    @FXML
    TextField filterTextField;
    @FXML
    RadioButton groupFilterCriteriaRadioButton;
    @FXML
    RadioButton studentFilterCriteriaRadioButton;
    @FXML
    RadioButton homeworkFilterCriteriaRadioButton;
    @FXML
    RadioButton labTeacherFilterCriteriaRadioButton;
    @FXML
    ToggleGroup filterCriteriaGradesView;
    @FXML
    TableView gradesTableView;
    @FXML
    TableColumn studentTableViewColumn;
    @FXML
    TableColumn gradeTableViewColumn;
    @FXML
    TableColumn homeworkTableViewColumn;
    @FXML
    TableColumn groupTableViewColumn;
    @FXML
    TableColumn teacherTableViewColumn;

    //Feedback template tab
    @FXML
    Tab feedbackTemplateTab;
    @FXML
    TextArea templateTextArea;

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void setHomeworkService(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setGradeService(GradeService gradeService) {
        this.gradeService = gradeService;
    }

    public void setUserFeedbackTemplateService(UserFeedbackTemplateService userFeedbackTemplateService) {
        this.userFeedbackTemplateService = userFeedbackTemplateService;
    }

    private void initializeView(Boolean studentModelChanged, Boolean homeworkModelChanged, Boolean gradesModelChanged) {

        //gradesTab.getSelectionModel().select(viewGradesTab);
        if(!addGradeTab.isDisable()) {
            gradeTextField.setText(null);
            feedbackTextArea.setText(null);
            initializeStudentsView(studentModelChanged);
            initializeHomeworkView(homeworkModelChanged);
        }
        if(!viewGradesTab.isDisable())
            initializeGradesView(gradesModelChanged);

        if(!feedbackTemplateTab.isDisable())
            initializeFeedbackTemplateView();
    }

    private void initializeFeedbackTemplateView() {
        try {
            templateTextArea.setText(userFeedbackTemplateService.getUserFeedbackTemplate(user.getID()).getFeedbackTemplate());
        }
        catch (InexistentUserFeedbackTemplate e) {

        }
    }

    private void initializeGradesView(Boolean gradesModelChanged) {
        if(gradesModelChanged)
            initializeGradesModel();

        ObservableList<GradeDTO> gradeDTOS = FXCollections.observableArrayList();
        String filter = filterTextField.getText();
        RadioButton selectedFilter = (RadioButton) filterCriteriaGradesView.getSelectedToggle();
        Predicate<Grade> filterPredicate = x -> true;
        if(selectedFilter.equals(groupFilterCriteriaRadioButton)) {
            try {
                Short.valueOf(filter);
                filterPredicate = x -> x.getStudent().getGroup().equals(Short.valueOf(filter));
            }
            catch (NumberFormatException e) {

            }
        }
        else if(selectedFilter.equals(studentFilterCriteriaRadioButton)) {
            filterPredicate = x -> x.getStudent().getName().contains(filter);
        }
        else if(selectedFilter.equals(homeworkFilterCriteriaRadioButton)) {
            filterPredicate = x -> x.getHomework().getDescription().contains(filter);
        }
        else {
            filterPredicate = x -> x.getStudent().getLabTeacherName().contains(filter);
        }

        gradeDTOS.setAll(grades.stream()
                .filter(filterPredicate)
                .map(x -> new GradeDTO(x.getStudent().getID(), x.getHomework().getID(), x.getStudent().getName(), x.getStudent().getGroup(),
                        x.getHomework().getDescription(), x.getValue(), x.getStudent().getLabTeacherName()))
                .collect(Collectors.toList()));
        gradesTableView.setItems(gradeDTOS);
    }

    private void initializeGradesModel() {
        grades.setAll(StreamSupport.stream(
                gradeService.getAllGrades().spliterator(), false)
                .collect(Collectors.toList()));
    }

    private void initializeHomeworkView(Boolean homeworkModelChanged) {
        if(homeworkModelChanged)
            initializeHomeworkModel();
        final String filterText;
        Object homeworkComboBoxValue = homeworkComboBox.getValue();
        if(homeworkComboBoxValue == null || homeworkComboBoxValue instanceof HomeworkDTO)
            filterText = "";
        else
            filterText = (String) homeworkComboBoxValue;
        //System.out.println(filterText);
        ObservableList<HomeworkDTO> homeworkDTOS = FXCollections.observableArrayList();
        homeworkDTOS.setAll(homework.stream()
                .filter(x -> x.getDescription().contains(filterText))
                .map(x -> new HomeworkDTO(x.getID(), x.getDescription()))
                .collect(Collectors.toList()));
        homeworkComboBox.setItems(homeworkDTOS);
        try {
            homeworkComboBox.setValue(homeworkDTOS.stream()
                    .filter(x -> homeworkService.getHomework(x.getId()).getDeadlineWeek().equals(
                            WeekManager.getInstance().getCurrentWeek()))
                    .collect(Collectors.toList())
                    .get(0));
        }
        catch (IndexOutOfBoundsException ex) {

        }
    }

    private void initializeStudentsView(Boolean studentModelChanged) {
        if(studentModelChanged)
            initializeStudentsModel();
        final String filterText;
        Object studentComboBoxValue = studentComboBox.getValue();
        if(studentComboBoxValue == null || studentComboBoxValue instanceof StudentDTO)
            filterText = "";
        else
            filterText = (String) studentComboBoxValue;
        //System.out.println(filterText);
        ObservableList<StudentDTO> studentDTOS = FXCollections.observableArrayList();
        studentDTOS.setAll(students.stream()
                .filter(x -> x.getLabTeacherName().equals(user.getName()))
                .filter(x -> x.getName().contains(filterText))
                .map(x -> new StudentDTO(x.getID(), x.getName()))
                .collect(Collectors.toList()));
        studentComboBox.setItems(studentDTOS);
    }

    private void initializeHomeworkModel() {
        homework.setAll(StreamSupport.stream(
                homeworkService.getAllHomework().spliterator(), false)
                .collect(Collectors.toList()));
    }

    private void initializeStudentsModel() {
        students.setAll(StreamSupport.stream(
                studentService.getAllStudents().spliterator(), false)
                .collect(Collectors.toList()));
    }

    private void filterStudentsAction() {
        if(filterStudentsCheckBox.isSelected()) {
            studentComboBox.setEditable(true);
            studentComboBox.setValue("");
            initializeStudentsView(false);
        }
        else {
            studentComboBox.setEditable(false);
        }
    }

    private boolean validateAdd() {
        Boolean errors = false;
        String errorStyle = "-fx-border-color: red; -fx-border-width: 2px;";
        Object studentComboBoxValue = studentComboBox.getValue();
        Object homeworkComboBoxValue = homeworkComboBox.getValue();
        String grade = gradeTextField.getText();

        if(!(studentComboBoxValue instanceof StudentDTO)) {
            studentErrorLabel.setText("Invalid student selection!");
            studentComboBox.setStyle(errorStyle);
            errors = true;
        }
        if(!(homeworkComboBoxValue instanceof HomeworkDTO)) {
            homeworkErrorLabel.setText("Invalid homework selection!");
            homeworkComboBox.setStyle(errorStyle);
            errors = true;
        }
        if(grade == null || grade.isEmpty()) {
            gradeErrorLabel.setText("Invalid grade introduced!");
            gradeTextField.setStyle(errorStyle);
            errors = true;
        }
        else
            try {
                Double value = Double.valueOf(grade);
                if(value < 0 || value > 10)
                    throw new NumberFormatException("");
            }
            catch (NumberFormatException e) {
                gradeErrorLabel.setText("Invalid grade introduced!");
                gradeTextField.setStyle(errorStyle);
                errors = true;
            }
        return !errors;
    }

    private void addGradeAction() throws IOException {
        Object studentComboBoxValue = studentComboBox.getValue();
        Object homeworkComboBoxValue = homeworkComboBox.getValue();
        String grade = gradeTextField.getText();
        String feedback = feedbackTextArea.getText();
        Double penalty = gradeService.getPossiblePenalty(((StudentDTO) studentComboBoxValue).getId(),
                ((HomeworkDTO) homeworkComboBoxValue).getId());
        if(!studentService.getStudent(((StudentDTO) studentComboBoxValue).getId())
                .getLabTeacherName().equals(user.getName()))
            throw new CannotAddGradeException("You don't have the permission to add grade for this student!");
        if(penalty == -1)
            throw new CannotAddGradeException("Cannot add a grade! This homework is graded by default with 1!");

        ButtonType res =
                AlertViewController.showAlert(Alert.AlertType.WARNING, "A new grade with the following " +
                "specifications is going to be added:\n" +
                "Homework: " + ((HomeworkDTO) homeworkComboBoxValue).getDescription() + "\n" +
                "Student: " + ((StudentDTO) studentComboBoxValue).getName() + "\n" +
                "Desired value: " + grade + "(Penalty to be added: "
                + penalty + ")\n" +
                "Do you want the specified grade to be added?");

        if (res == ButtonType.OK) {
            gradeService.addGrade(((StudentDTO) studentComboBoxValue).getId(),
                    ((HomeworkDTO) homeworkComboBoxValue).getId(), Double.parseDouble(grade), feedback);
        }
        else throw new NoSuchElementException("No element added!");

    }

    public void handleAddImageViewOnAction(MouseEvent actionEvent) throws IOException {
        if(validateAdd())
            try {
                addGradeAction();
                AlertViewController.showAlert(Alert.AlertType.INFORMATION, "Grade successfully added!");
            }
            catch (CannotAddGradeException | ValidationException | ExistentGradeException | IOException e) {
                AlertViewController.showAlert(Alert.AlertType.ERROR, e.getMessage());
            }
            catch (NoSuchElementException e) {

            }
        }


    @Override
    public void update(EntityChangeEvent entityChangeEvent) {
        initializeView(true,
                true, true);
    }

    public void handleHomeworkComboBoxKeyTyped(KeyEvent keyEvent) {
        initializeHomeworkView(false);
    }

    public void handleStudentsComboBoxKeyTyped(KeyEvent keyEvent) {
        //System.out.println(keyEvent.getCode());
        if(keyEvent.getCode().equals(KeyCode.CONTROL)) {

            initializeStudentsView(false);
            studentComboBox.setEditable(false);
            filterStudentsCheckBox.setSelected(false);
        }
    }

    public void handleHomeworkComboBoxOnAction(ActionEvent actionEvent) {
        homeworkErrorLabel.setText(null);
        homeworkComboBox.setStyle(null);
        addPenaltyFeedback();
        //addGradeButton.setDisable(checkExistingGrade());
    }

    public void addPenaltyFeedback() {
        Object studentComboBoxValue = studentComboBox.getValue();
        Object homeworkComboBoxValue = homeworkComboBox.getValue();
        if(studentComboBoxValue instanceof StudentDTO && homeworkComboBoxValue instanceof HomeworkDTO) {
            Double penalty = gradeService.getPossiblePenalty(((StudentDTO) studentComboBoxValue).getId(),
                    ((HomeworkDTO) homeworkComboBoxValue).getId());
            if(penalty > 0)
                feedbackTextArea.setText(feedbackTextArea.getText() + "\nGRADE WAS DECREASED BY " + penalty.toString() +
                        "POINTS FOR DELAY\n");
            else
                feedbackTextArea.setText("");
        }
    }

    private Boolean checkExistingGrade() {
        Object studentComboBoxValue = studentComboBox.getValue();
        Object homeworkComboBoxValue = homeworkComboBox.getValue();

        if(studentComboBoxValue instanceof StudentDTO && homeworkComboBoxValue instanceof HomeworkDTO &&
                gradeService.getGrade(((StudentDTO) studentComboBoxValue).getId(),
                        ((HomeworkDTO) homeworkComboBoxValue).getId()) != null)
            return true;
        return false;
    }

    public void handleStudentComboBoxOnAction(ActionEvent actionEvent) {
        studentErrorLabel.setText(null);
        studentComboBox.setStyle(null);
        addPenaltyFeedback();
        //addGradeButton.setDisable(checkExistingGrade());
    }

    public void handleFilterStudentsCheckBoxOnAction(ActionEvent actionEvent) {
        filterStudentsAction();
    }


    public void handleClearFormImageViewOnAction(MouseEvent actionEvent) {
        homeworkComboBox.setValue(null);
        studentComboBox.setValue(null);
        gradeTextField.clear();
        feedbackTextArea.clear();
    }

    public void handleFilterEntriesCheckBoxOnAction(ActionEvent actionEvent) {
        CheckBox filter = (CheckBox) actionEvent.getSource();
        setFilterComponents(!filter.isSelected());
        if(!filter.isSelected())
            initializeGradesView(false);
    }

    private void setFilterComponents(Boolean b) {
        filterTextField.clear();
        filterTextField.setDisable(b);
        groupFilterCriteriaRadioButton.setDisable(b);
        studentFilterCriteriaRadioButton.setDisable(b);
        homeworkFilterCriteriaRadioButton.setDisable(b);
        labTeacherFilterCriteriaRadioButton.setDisable(b);
    }

    public void handleFilterTextFieldKeyPressed(KeyEvent keyEvent) {
        //System.out.println(keyEvent.getCode());
        if(keyEvent.getCode() == KeyCode.ENTER)
            initializeGradesView(false);
    }


    public void handleGradeTextFieldOnKeyTyped(KeyEvent keyEvent) {
        gradeErrorLabel.setText(null);
        gradeTextField.setStyle(null);
    }

    public void handlePasteTemplateImageViewOnMouseClicked(MouseEvent mouseEvent) {
        try {
            feedbackTextArea.setText(userFeedbackTemplateService.getUserFeedbackTemplate(user.getID()).getFeedbackTemplate() +
                    '\n' + (feedbackTextArea.getText() == null ? "" : feedbackTextArea.getText()));
        }
        catch (InexistentUserFeedbackTemplate e) {

        }
    }

    public void handleClearTemplateTextAreaOnMouseClicked(MouseEvent mouseEvent) {
        templateTextArea.clear();
    }

    public void handleUpdateFeedbackTemplateImageViewOnMouseClicked(MouseEvent mouseEvent) {
        String template = templateTextArea.getText();
        if(template.length() == 0)
            userFeedbackTemplateService.deleteFeedbackTemplate(user.getID());
        else
            userFeedbackTemplateService.addOrUpdateUserFeedbackTemplate(user.getID(), template);
    }

    public void handleDeleteGradeOnAction(ActionEvent actionEvent) throws IOException {
        GradeDTO selected = (GradeDTO)gradesTableView.getSelectionModel().getSelectedItem();
        if(selected == null)
            return;
        if(!selected.getTeacher().equals(user.getName()))
            AlertViewController.showAlert(Alert.AlertType.ERROR, "You have no permission to delete this homework!");
        else
            gradeService.deleteGrade(selected.getStudentID(), selected.getHomeworkID());
    }

    public void handleUpdateGradeTextFieldOnKeyPressed(KeyEvent keyEvent) throws IOException {
        if(keyEvent.getCode() == KeyCode.ENTER) {
            GradeDTO selected = (GradeDTO) gradesTableView.getSelectionModel().getSelectedItem();
            TextField t = ((TextField)keyEvent.getSource());
            if(selected == null)
                return;
            if(!selected.getTeacher().equals(user.getName()))
                AlertViewController.showAlert(Alert.AlertType.ERROR, "You have no permission to update this homework!");
            else
                gradeService.updateGrade(selected.getStudentID(), selected.getHomeworkID(), Double.valueOf(t.getText()));
            t.setText(null);
        }
    }

    public void init() {
        switch (user.getUserRole()) {
            case TEACHER:
                addGradeTab.setDisable(false);
                viewGradesTab.setDisable(false);
                break;
            case SECRETARY:
                addGradeTab.setDisable(true);
                feedbackTemplateTab.setDisable(true);
                viewGradesTab.setDisable(false);
                break;
            default:
                break;
        }

        studentTableViewColumn.setCellValueFactory(new PropertyValueFactory<GradeDTO, String>("student"));
        groupTableViewColumn.setCellValueFactory(new PropertyValueFactory<GradeDTO, Short>("group"));
        homeworkTableViewColumn.setCellValueFactory(new PropertyValueFactory<GradeDTO, String>("homework"));
        gradeTableViewColumn.setCellValueFactory(new PropertyValueFactory<GradeDTO, Double>("grade"));
        teacherTableViewColumn.setCellValueFactory(new PropertyValueFactory<GradeDTO, String>("teacher"));

        initializeView(true, true, true);
    }


}
