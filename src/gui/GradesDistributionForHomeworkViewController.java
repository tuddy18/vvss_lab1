package gui;

import domain.HasID;
import domain.Homework;
import domain.HomeworkDTO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ComboBox;
import javafx.scene.input.MouseEvent;
import service.GradeService;
import service.HomeworkService;
import service.StudentService;
import utilities.EntityChangeEvent;
import utilities.Observer;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GradesDistributionForHomeworkViewController implements Observer<EntityChangeEvent<HasID>> {
    private GradeService gradeService;
    private HomeworkService homeworkService;
    private StudentService studentService;
    private ObservableList<HomeworkDTO> homeworkDTOS = FXCollections.observableArrayList();
    @FXML
    ComboBox selectHomeworkComboBox;
    @FXML
    BarChart gradeDistributionBarChart;

    private void initializeView(Boolean modelChanged) {
        if(modelChanged)
            initializeModel();

        selectHomeworkComboBox.setItems(homeworkDTOS);
        gradeDistributionBarChart.setLegendVisible(false);
    }

    private void initializeModel() {
        homeworkDTOS.setAll(StreamSupport.stream(homeworkService.getAllHomework().spliterator(), false)
                .map(h -> new HomeworkDTO(h.getID(), h.getDescription()))
                .collect(Collectors.toList()));
    }

    private void selectHomeworkAction() {
        Integer id = ((HomeworkDTO) selectHomeworkComboBox.getSelectionModel().getSelectedItem()).getId();
        Map<Double, Long> gradeDist = gradeService.getGradesDistributionForHomework(id);
        //gradeDist.forEach((x, y) -> System.out.println(x.toString() + " " + y.toString()));
        XYChart.Series dataSeries = new XYChart.Series();
        gradeDistributionBarChart.getData().clear();
        ((CategoryAxis)gradeDistributionBarChart.getXAxis()).getCategories().clear();
        List<String> categories =  StreamSupport.stream(gradeDist.keySet().spliterator(), false)
                .sorted()
                .map(String::valueOf)
                .collect(Collectors.toList());
        ((CategoryAxis)gradeDistributionBarChart.getXAxis())
                .setCategories(FXCollections.observableArrayList(categories));
        gradeDist.forEach((x, y) -> dataSeries.getData().add(new XYChart.Data(x.toString(), y)));
        gradeDistributionBarChart.getData().add(dataSeries);
    }

    public void setGradeService(GradeService gradeService) {
        this.gradeService = gradeService;
    }


    public void setHomeworkService(HomeworkService homeworkService) {
        this.homeworkService = homeworkService;
    }

    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    public void handleSelectHomeworkComboBoxOnAction(ActionEvent actionEvent) {
        selectHomeworkAction();
    }

    @Override
    public void update(EntityChangeEvent<HasID> changeEvent) {
        initializeView(true);
    }

    public void init() {
        initializeView(true);
    }
}
