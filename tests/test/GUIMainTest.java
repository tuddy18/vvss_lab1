package test;

import domain.User;
import gui.LoginWindowController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import main.GUIMain;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.testfx.api.FxToolkit;
import org.testfx.framework.junit.ApplicationTest;
import repository.CrudRepository;
import repository.db.UserDatabaseRepository;
import service.UserService;
import validation.UserValidator;
import validation.Validator;

import static org.junit.Assert.*;

public class GUIMainTest extends ApplicationTest {
    @Override
    public void start (Stage stage) throws Exception {
        LoginWindowController loginWindowController;
        Validator<User> userValidator = new UserValidator();
        CrudRepository<String, User> userCrudRepository =
                new UserDatabaseRepository("Users", userValidator, true);
        UserService userService = new UserService(userCrudRepository);
        FXMLLoader loginWindowLoader = new FXMLLoader();
        loginWindowLoader.setLocation(getClass().getResource(
                "/resources/views/loginWindow.fxml"));
        BorderPane rootLayout = loginWindowLoader.load();
        loginWindowController = loginWindowLoader.getController();
        Scene loginScene = new Scene(rootLayout);
        stage.setScene(loginScene);
        loginWindowController.setPrimaryStage(stage);
        loginWindowController.setUserService(userService);
        stage.initStyle(StageStyle.UNDECORATED);
        stage.show();
        stage.toFront();
    }

    @Before
    public void setUp () throws Exception {
    }

    @After
    public void tearDown () throws Exception {
        FxToolkit.hideStage();
        release(new KeyCode[]{});
        release(new MouseButton[]{});
    }

    @Test
    public void testLogin () {
        clickOn("#usernameTextBox");
        write("profu");
        clickOn("#passwordField");
        write("1234");
        press(KeyCode.ENTER);
        Label roleLabel = (Label) GuiTest.find("#roleLabel");
        Label welcomeLabel = (Label) GuiTest.find("#welcomeLabel");

        assertTrue(roleLabel.getText().contains("TEACHER"));
        assertTrue(welcomeLabel.getText().contains("profu"));
    }

    @Test
    public void testIoanBadFilter(){
        clickOn("#usernameTextBox");
        write("profu");
        clickOn("#passwordField");
        write("1234");
        press(KeyCode.ENTER);
        clickOn("#gradesImageView");
        clickOn("#filterEntriesCheckBox");

        clickOn("#studentCriteriaRadioButton");
        clickOn("#filterTextField");
        write("ioan");
        write('\n');


        TableView table = GuiTest.find("#gradesTableView");
        assertEquals(5, table.getItems().size());
    }

    @Test
    public void testIoanGoodFilter(){
        clickOn("#usernameTextBox");
        write("profu");
        clickOn("#passwordField");
        write("1234");
        press(KeyCode.ENTER);
        clickOn("#gradesImageView");
        clickOn("#filterEntriesCheckBox");

        clickOn("#studentCriteriaRadioButton");
        clickOn("#filterTextField");
        write("Ioan");
        write('\n');


        TableView table = GuiTest.find("#gradesTableView");
        assertEquals(5, table.getItems().size());
    }

    @Test
    public void testIoanGroupBadFilter(){
        clickOn("#usernameTextBox");
        write("profu");
        clickOn("#passwordField");
        write("1234");
        press(KeyCode.ENTER);
        clickOn("#gradesImageView");
        clickOn("#filterEntriesCheckBox");

        clickOn("#groupFilterCriteriaRadioButton");
        clickOn("#filterTextField");
        write("Ioan");
        write('\n');

        TableView table = GuiTest.find("#gradesTableView");
        assertEquals(5, table.getItems().size());
    }
}